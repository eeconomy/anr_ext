﻿#Область ПечатьСлужебные

&ИзменениеИКонтроль("СформироватьПечатнуюФормуНакладнаяБСО")
// EE:ANKT-123, BOP
// Добавление заполнения в печатной форме приходно-расходной накладной бсо подотчетного лица - получил,
// кому отпущено и через кого
//(
Функция ТК_СформироватьПечатнуюФормуНакладнаяБСО(МассивОбъектов, ОбъектыПечати, КомплектыПечати)
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("МассивОбъектов", МассивОбъектов);
	Запрос.Текст =
	"ВЫБРАТЬ
	|	ПеремещениеТоваров.Ссылка КАК Ссылка,
	|	ПеремещениеТоваров.Дата КАК Дата,
	|	ПеремещениеТоваров.Номер КАК Номер,
	|	ПеремещениеТоваров.Организация КАК Организация,
	|	ПРЕДСТАВЛЕНИЕ(ПеремещениеТоваров.Организация.НаименованиеПолное) КАК ОрганизацияНаименованиеПолное,
	|	ПеремещениеТоваров.Организация КАК ОрганизацияПолучатель,
	|	ПРЕДСТАВЛЕНИЕ(ПеремещениеТоваров.Организация.НаименованиеПолное) КАК ОрганизацияПолучательНаименованиеПолное,
	#Удаление
	|	НЕОПРЕДЕЛЕНО КАК СкладПолучатель,
	#КонецУдаления
	#Вставка
	|	ПеремещениеТоваров.Подразделение КАК СкладПолучатель,
	#КонецВставки
	|	НЕОПРЕДЕЛЕНО КАК СкладПолучательПодразделение,
	|	НЕОПРЕДЕЛЕНО КАК СкладПолучательДолжность,
	#Удаление
	|	НЕОПРЕДЕЛЕНО КАК СкладПолучательОтветственный,
	#КонецУдаления
	#Вставка
	|	ПеремещениеТоваров.Подразделение.ТекущийРуководитель КАК СкладПолучательОтветственный,
	|   ТекущиеКадровыеДанныеСотрудников.ФизическоеЛицо КАК ФизическоеЛицо,
	|	ТекущиеКадровыеДанныеСотрудников.ТекущаяДолжность КАК ТекущаяДолжность,
	#КонецВставки
	|	ПеремещениеТоваров.Склад КАК СкладОтправитель,
	|	ПРЕДСТАВЛЕНИЕ(ПеремещениеТоваров.Склад.Подразделение) КАК СкладОтправительПодразделение,
	|	ПеремещениеТоваров.Склад.ТекущаяДолжностьОтветственного КАК СкладОтправительДолжность,
	|	ПеремещениеТоваров.Склад.ТекущийОтветственный КАК СкладОтправительОтветственный
	|ИЗ
	|	Документ.ВнутреннееПотреблениеТоваров КАК ПеремещениеТоваров
	#Вставка
	|	ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ТекущиеКадровыеДанныеСотрудников КАК ТекущиеКадровыеДанныеСотрудников
	|	ПО (ПеремещениеТоваров.Подразделение.ТекущийРуководитель = ТекущиеКадровыеДанныеСотрудников.ФизическоеЛицо)
	#КонецВставки
	|ГДЕ
	|	ПеремещениеТоваров.Ссылка В(&МассивОбъектов)
	|
	|УПОРЯДОЧИТЬ ПО
	|	Ссылка
	|АВТОУПОРЯДОЧИВАНИЕ
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ПеремещениеТоваровТовары.Ссылка КАК Ссылка,
	|	ПеремещениеТоваровТовары.Номенклатура.НаименованиеПолное КАК Номенклатура,
	|	1 КАК КоличествоКниг,
	|	ПеремещениеТоваровТовары.Количество КАК Количество,
	|	ПеремещениеТоваровТовары.СерияБланкаСтрогойОтчетности КАК Серия,
	|	ПеремещениеТоваровТовары.НачальныйНомерБланкаСтрогойОтчетности КАК НачальныйНомер,
	|	ПеремещениеТоваровТовары.КонечныйНомерБланкаСтрогойОтчетности КАК КонечныйНомер
	|ИЗ
	|	Документ.ВнутреннееПотреблениеТоваров.Товары КАК ПеремещениеТоваровТовары
	|ГДЕ
	|	ПеремещениеТоваровТовары.Ссылка В(&МассивОбъектов)
	|
	|УПОРЯДОЧИТЬ ПО
	|	Ссылка,
	|	ПеремещениеТоваровТовары.НомерСтроки
	|АВТОУПОРЯДОЧИВАНИЕ";
	
	Результат = Запрос.ВыполнитьПакет();
	Если Результат[0].Пустой() Или Результат[1].Пустой() Тогда
		Возврат Неопределено;
	КонецЕсли;

	ТабДокумент = Новый ТабличныйДокумент;
	ТабДокумент.ИмяПараметровПечати = "ПАРАМЕТРЫ_ПЕЧАТИ_ВнутреннееПотреблениеТоваров_НакладнаяБСО";
	ТабДокумент.АвтоМасштаб = Истина;
	ТабДокумент.ОриентацияСтраницы = ОриентацияСтраницы.Портрет;
	ТабДокумент.ПолеСверху = 5;
	ТабДокумент.ПолеСлева = 10;
	ТабДокумент.ПолеСнизу = 5;
	ТабДокумент.ПолеСправа = 5;

	Макет = УправлениеПечатью.МакетПечатнойФормы("Документ.ВнутреннееПотреблениеТоваров.ПФ_MXL_НакладнаяБСО");

	ПервыйДокумент = Истина;

	Шапка = Результат[0].Выбрать();
	ВыборкаНоменклатура = Результат[1].Выбрать();
	
	Пока Шапка.Следующий() Цикл
		Если Не ПервыйДокумент Тогда
			ТабДокумент.ВывестиГоризонтальныйРазделительСтраниц();
		КонецЕсли;

		ПервыйДокумент = Ложь;
		НомерСтрокиНачало = ТабДокумент.ВысотаТаблицы + 1;

		ОбластьМакетаШапка = Макет.ПолучитьОбласть("Шапка");
		ОбластьМакетаСтрока = Макет.ПолучитьОбласть("Строка");
		ОбластьМакетаПодвал = Макет.ПолучитьОбласть("Подвал");

		#Удаление
		ОбластьМакетаШапка.Параметры.ФиоПолучил = ФизическиеЛицаУТ.ФамилияИнициалыФизЛица(Шапка.СкладПолучательОтветственный);
		#КонецУдаления
		#Вставка
		ОбластьМакетаШапка.Параметры.ФиоПолучил = Шапка.СкладПолучатель;
		ОбластьМакетаШапка.Параметры.ЧерезКого = Шапка.СкладОтправитель;
		#КонецВставки

		ОбластьМакетаШапка.Параметры.ОрганизацияОтправитель = Шапка.ОрганизацияНаименованиеПолное + ", " + Шапка.СкладОтправительПодразделение;
		Если ЗначениеЗаполнено(Шапка.ОрганизацияПолучательНаименованиеПолное) Тогда
			ОбластьМакетаШапка.Параметры.ОрганизацияПолучатель = Шапка.ОрганизацияПолучательНаименованиеПолное + ", " + Шапка.СкладПолучательПодразделение;
		Иначе
			ОбластьМакетаШапка.Параметры.ОрганизацияПолучатель = Шапка.ОрганизацияНаименованиеПолное + ", " + Шапка.СкладПолучательПодразделение;
		КонецЕсли;

		ОбластьМакетаШапка.Параметры.НомерДок = ПрефиксацияОбъектовКлиентСервер.НомерНаПечать(Шапка.Номер, Истина, Истина);
		ОбластьМакетаШапка.Параметры.ДатаДок = Шапка.Дата;
		ТабДокумент.Вывести(ОбластьМакетаШапка);

		НомерСтроки = 1;

		ВыборкаНоменклатура.Сбросить();
		СтуктураПоиска = Новый Структура("Ссылка", Шапка.Ссылка);
		Пока ВыборкаНоменклатура.НайтиСледующий(СтуктураПоиска) Цикл
			ОбластьМакетаСтрока.Параметры.Заполнить(ВыборкаНоменклатура);
			ОбластьМакетаСтрока.Параметры.Серия = СокрЛП(ВыборкаНоменклатура.Серия);
			ОбластьМакетаСтрока.Параметры.НомерСтроки = НомерСтроки;
			ТабДокумент.Вывести(ОбластьМакетаСтрока);

			НомерСтроки = НомерСтроки + 1;
		КонецЦикла;

		ОбластьМакетаПодвал.Параметры.ОтправительМОЛДолжность = Шапка.СкладОтправительДолжность;
		ОбластьМакетаПодвал.Параметры.ОтправительМОЛ = ФизическиеЛицаУТ.ФамилияИнициалыФизЛица(Шапка.СкладОтправительОтветственный);
		
		#Удаление
		ОбластьМакетаПодвал.Параметры.ПолучательМОЛДолжность = Шапка.СкладПолучательДолжность;
		#КонецУдаления
		#Вставка
		ОбластьМакетаПодвал.Параметры.ПолучательМОЛДолжность = Шапка.ТекущаяДолжность;
		#КонецВставки
		ОбластьМакетаПодвал.Параметры.ПолучательМОЛ = ФизическиеЛицаУТ.ФамилияИнициалыФизЛица(Шапка.СкладПолучательОтветственный);

		ОтветственныеЛица = ОтветственныеЛицаБП.ОтветственныеЛица(Шапка.Организация, Шапка.Дата);
		ОбластьМакетаПодвал.Параметры.ГлБухгалтерДолжность = ОтветственныеЛица.ГлавныйБухгалтерДолжностьПредставление;
		ОбластьМакетаПодвал.Параметры.ГлБухгалтерМОЛ = ОтветственныеЛица.ГлавныйБухгалтерПредставление;
		ТабДокумент.Вывести(ОбластьМакетаПодвал);

		УправлениеПечатью.ЗадатьОбластьПечатиДокумента(ТабДокумент, НомерСтрокиНачало, ОбъектыПечати, Шапка.Ссылка);
	КонецЦикла;

	Возврат ТабДокумент;
КонецФункции
//)

&ИзменениеИКонтроль("СформироватьПечатнуюФормуАктСписанияМатериалов")
Функция ТК_СформироватьПечатнуюФормуАктСписанияМатериалов(МассивОбъектов, ОбъектыПечати, КомплектыПечати)

	ДанныеДляПечатнойФормы = ПолучитьДанныеДляАктСписанияМатериалов(МассивОбъектов);

	ВыборкаДокументы = ДанныеДляПечатнойФормы.РезультатПоШапке.Выбрать();
	ВыборкаСтрокиПоДокументам = ДанныеДляПечатнойФормы.РезультатПоТабличнойЧасти.Выбрать();

	ТабДокумент = Новый ТабличныйДокумент;
	ТабДокумент.ИмяПараметровПечати = "ПАРАМЕТРЫ_ПЕЧАТИ_ВнутреннееПотреблениеТоваров_АктСписанияМатериалов";
	ТабДокумент.АвтоМасштаб = Истина;
	ТабДокумент.ОриентацияСтраницы = ОриентацияСтраницы.Портрет;
	ТабДокумент.ПолеСверху = 5;
	ТабДокумент.ПолеСлева = 10;
	ТабДокумент.ПолеСнизу = 5;
	ТабДокумент.ПолеСправа = 5;

	Макет = УправлениеПечатью.МакетПечатнойФормы("Документ.ВнутреннееПотреблениеТоваров.ПФ_MXL_АктСписанияМатериалов");

	ПервыйДокумент = Истина;

	Шапка = ВыборкаДокументы;
	ВыборкаСсылка = ВыборкаСтрокиПоДокументам;

	Пока Шапка.Следующий() Цикл
		Если Не ПервыйДокумент Тогда
			ТабДокумент.ВывестиГоризонтальныйРазделительСтраниц();
		КонецЕсли;

		ПервыйДокумент = Ложь;
		НомерСтрокиНачало = ТабДокумент.ВысотаТаблицы + 1;

		ОтветственныеЛица = ОтветственныеЛицаБП.ОтветственныеЛица(Шапка.Организация, Шапка.ДатаДокумента);

		ОблЗаголовок = Макет.ПолучитьОбласть("Заголовок");
		ОблСтрокаКомиссии = Макет.ПолучитьОбласть("СтрокаКомиссии");
		ОблШапка = Макет.ПолучитьОбласть("Шапка");
		ОблИтоги = Макет.ПолучитьОбласть("Итоги");
		ОблПодвал = Макет.ПолучитьОбласть("Подвал");
		ОблСтрока = Макет.ПолучитьОбласть("Строка");
		ОбластьПодпись = Макет.ПолучитьОбласть("Подписи");
		ОбластьПодписиСтрока = Макет.ПолучитьОбласть("ПодписиСтрока");



		//ЗаполнитьЗаголовокКомиссии(Шапка, ОблЗаголовок);

		#Удаление
		ОблЗаголовок.Параметры.Дата = ТекущаяДатаСеанса();
		#КонецУдаления
		ОблЗаголовок.Параметры.ДатаДокумента = Формат(Шапка.ДатаДокумента, "ДЛФ=DD");
		ОблЗаголовок.Параметры.НомерДокумента = Шапка.Номер;

		ОблЗаголовок.Параметры.ПредставлениеОрганизации = ФормированиеПечатныхФорм.ОписаниеОрганизации(
		ФормированиеПечатныхФорм.СведенияОЮрФизЛице(Шапка.Организация, Шапка.ДатаДокумента), "СокращенноеНаименование, ИНН, ЮридическийАдрес");

		ОблЗаголовок.Параметры.УполномоченноеЛицо = ОтветственныеЛица.РуководительДолжностьПредставление + " " + ОтветственныеЛица.РуководительПредставление;

		ТабДокумент.Вывести(ОблЗаголовок);

		//ЗаполнитьКомиссии(Шапка, ОблСтрокаКомиссии, ТабДокумент);

		//ОблШапка.Параметры.МесяцДокумента = СклонениеПредставленийОбъектов.ПросклонятьПредставление(НРег(Формат(Шапка.ДатаДокумента, "ДФ=ММММ")), 6);
		//ОблШапка.Параметры.ГодДокумента = Формат(Шапка.ДатаДокумента, "ДФ=гггг");
		//ОблШапка.Параметры.Подразделение = Шапка.Подразделение;
		#Вставка
		// 4D:ТКАнрекс, МаксимК,  
		// Доработка и ошибка. Оплата 50%. Печатная форма Списание на расходы,№ANKT-6
		// {
		ОблШапка.Параметры.Комментарий = Шапка.Комментарий; 
		// } 4D
		#КонецВставки

		ТабДокумент.Вывести(ОблШапка);

		СтоимостьВсего = 0;
		НомерСтроки = 0;
		КоличествоИтого = 0;
		ВыборкаСсылка.Сбросить();
		СтруктураПоиска = Новый Структура("Ссылка", Шапка.Ссылка);

		Пока ВыборкаСсылка.НайтиСледующий(СтруктураПоиска) Цикл
			Если Не ЗначениеЗаполнено(ВыборкаСсылка.Номенклатура) Тогда
				Продолжить;
			КонецЕсли; 
			СтоимостьВсего = СтоимостьВсего + ВыборкаСсылка.СтоимостьВсего;
			КоличествоИтого = КоличествоИтого + ВыборкаСсылка.Количество;
			НомерСтроки = НомерСтроки + 1;
			ОблСтрока.Параметры.Заполнить(ВыборкаСсылка);
			ОблСтрока.Параметры.НомерСтроки = НомерСтроки;
			ОблСтрока.Параметры.Упаковка = ?(ЗначениеЗаполнено(ВыборкаСсылка.Упаковка), ВыборкаСсылка.Упаковка, ВыборкаСсылка.Номенклатура.ЕдиницаИзмерения);
			ТабДокумент.Вывести(ОблСтрока);
		КонецЦикла;

		ОблИтоги.Параметры.СуммаИтого = СтоимостьВсего;
		ОблИтоги.Параметры.КоличествоИтого = КоличествоИтого;
		ОблИтоги.Параметры.КоличествоИтогоПрописью = НРег(ЧислоПрописью(КоличествоИтого,, "шт, шт, шт, м,,,,, 0"));

		ТабДокумент.Вывести(ОблИтоги);

		ТабДокумент.Вывести(ОблПодвал);

		// 4D:ERP для Беларуси, Антон, 12.03.2022 14:40:01 
		// Вывести комиссию на печатную форму, №32112
		// {
		Если ЗначениеЗаполнено(Шапка.Комиссия) Тогда
			НомерЧлена = 0;
			ФизЛицоПредседателя = Шапка.Комиссия.Председатель;
			ОбластьПодпись.Параметры.Должность = Шапка.Комиссия.ДолжностьДляПечати;
			ОбластьПодпись.Параметры.Председатель = Строка(ФизЛицоПредседателя.Фамилия) + " " 
			+ ФизическиеЛицаЗарплатаКадрыКлиентСервер.ИнициалыИмени(ФизЛицоПредседателя.Имя) 
			+ ФизическиеЛицаЗарплатаКадрыКлиентСервер.ИнициалыИмени(ФизЛицоПредседателя.Отчество);
			ОбластьПодпись.Параметры.Председатель = СокрЛП(ОбластьПодпись.Параметры.Председатель); 
			ТабДокумент.Вывести(ОбластьПодпись);
			Для Каждого  СтрокаКомиссии Из Шапка.Комиссия.Комиссия  Цикл
				НомерЧлена = НомерЧлена + 1;
				Если НомерЧлена = 1 Тогда
					ОбластьПодписиСтрока.Параметры.ТекстКомиссииПодпись = "Члены комиссии:";	
				Иначе
					ОбластьПодписиСтрока.Параметры.ТекстКомиссииПодпись = "";	
				КонецЕсли; 
				ОбластьПодписиСтрока.Параметры.ФИОЧленКомиссии = Строка(СтрокаКомиссии.Сотрудник.Фамилия) + " " 
				+ ФизическиеЛицаЗарплатаКадрыКлиентСервер.ИнициалыИмени(СтрокаКомиссии.Сотрудник.Имя) 
				+ ФизическиеЛицаЗарплатаКадрыКлиентСервер.ИнициалыИмени(СтрокаКомиссии.Сотрудник.Отчество);
				ОбластьПодписиСтрока.Параметры.ФИОЧленКомиссии = СокрЛП(ОбластьПодписиСтрока.Параметры.ФИОЧленКомиссии);	
				ОбластьПодписиСтрока.Параметры.ДолжностьЧленКомиссии = СтрокаКомиссии.ДолжностьДляПечати;
				ТабДокумент.Вывести(ОбластьПодписиСтрока);
			КонецЦикла;
		Иначе
			ТабДокумент.Вывести(ОбластьПодпись);
			НомерЧлена=0;
			Для Счетчик = 1 По 3 Цикл
				НомерЧлена = НомерЧлена + 1;
				Если НомерЧлена = 1 Тогда
					ОбластьПодписиСтрока.Параметры.ТекстКомиссииПодпись = "Члены комиссии:";	
				Иначе
					ОбластьПодписиСтрока.Параметры.ТекстКомиссииПодпись = "";	
				КонецЕсли; 
				ТабДокумент.Вывести(ОбластьПодписиСтрока);
			КонецЦикла;
		КонецЕсли;
		// }
		// 4D


		//ЗаполнитьПодписи(Шапка, ОблПодписи, ОблПодписиСтрока, ТабДокумент);

		УправлениеПечатью.ЗадатьОбластьПечатиДокумента(ТабДокумент, НомерСтрокиНачало, ОбъектыПечати, Шапка.Ссылка);

	КонецЦикла;

	Возврат ТабДокумент;
КонецФункции

&ИзменениеИКонтроль("ПолучитьДанныеДляАктСписанияМатериалов")
Функция ТК_ПолучитьДанныеДляАктСписанияМатериалов(МассивДокументов)

	Запрос = Новый Запрос(
	"ВЫБРАТЬ
	|	Товары.Ссылка КАК Ссылка,
	|	Товары.НомерСтроки КАК НомерСтроки,
	|	Товары.Номенклатура КАК Номенклатура,
	|	Товары.Характеристика КАК Характеристика,
	|	Товары.Упаковка КАК Упаковка,
	|	Товары.Количество КАК Количество,
	|	Товары.КоличествоУпаковок КАК КоличествоУпаковок,
	|	КОНЕЦПЕРИОДА(Товары.Ссылка.Дата, ДЕНЬ) КАК ДатаПолученияЦены,
	|	Товары.Ссылка.ВидЦены КАК ВидЦены,
	|	Товары.Ссылка.ВидЦены.ВалютаЦены КАК ВалютаЦены
	|ПОМЕСТИТЬ Товары
	|ИЗ
	|	Документ.ВнутреннееПотреблениеТоваров.Товары КАК Товары
	|ГДЕ
	|	Товары.Ссылка В(&МассивДокументов)
	|	И Товары.Ссылка.ХозяйственнаяОперация = ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.СписаниеТоваровПоТребованию)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ПериодыЦенНоменклатуры.Ссылка КАК Ссылка,
	|	ПериодыЦенНоменклатуры.НомерСтроки КАК НомерСтроки,
	|	ЦеныНоменклатуры.Цена КАК Цена,
	|	ЦеныНоменклатуры.Упаковка КАК Упаковка
	|ПОМЕСТИТЬ Цены
	|ИЗ
	|	(ВЫБРАТЬ
	|		Товары.Ссылка КАК Ссылка,
	|		Товары.НомерСтроки КАК НомерСтроки,
	|		ЦеныНоменклатуры.ВидЦены КАК ВидЦены,
	|		ЦеныНоменклатуры.Номенклатура КАК Номенклатура,
	|		ЦеныНоменклатуры.Характеристика КАК Характеристика,
	|		МАКСИМУМ(ЦеныНоменклатуры.Период) КАК Период
	|	ИЗ
	|		Товары КАК Товары
	|			ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.ЦеныНоменклатуры КАК ЦеныНоменклатуры
	|			ПО Товары.ВидЦены = ЦеныНоменклатуры.ВидЦены
	|				И Товары.Номенклатура = ЦеныНоменклатуры.Номенклатура
	|				И Товары.Характеристика = ЦеныНоменклатуры.Характеристика
	|				И Товары.ДатаПолученияЦены >= ЦеныНоменклатуры.Период
	|				И Товары.ВалютаЦены = ЦеныНоменклатуры.Валюта
	|	
	|	СГРУППИРОВАТЬ ПО
	|		Товары.Ссылка,
	|		Товары.НомерСтроки,
	|		ЦеныНоменклатуры.ВидЦены,
	|		ЦеныНоменклатуры.Номенклатура,
	|		ЦеныНоменклатуры.Характеристика) КАК ПериодыЦенНоменклатуры
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.ЦеныНоменклатуры КАК ЦеныНоменклатуры
	|		ПО ПериодыЦенНоменклатуры.Период = ЦеныНоменклатуры.Период
	|			И ПериодыЦенНоменклатуры.ВидЦены = ЦеныНоменклатуры.ВидЦены
	|			И ПериодыЦенНоменклатуры.Номенклатура = ЦеныНоменклатуры.Номенклатура
	|			И ПериодыЦенНоменклатуры.Характеристика = ЦеныНоменклатуры.Характеристика
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	Документы.Ссылка КАК Ссылка,
	|	Документы.ХозяйственнаяОперация КАК ХозяйственнаяОперация,
	|	Документы.Номер КАК Номер,
	|	Документы.Дата КАК ДатаДокумента,
	|	Документы.Дата КАК ДатаСоставления,
	|	Документы.Организация КАК Организация,
	|	ПРЕДСТАВЛЕНИЕССЫЛКИ(Документы.Организация) КАК ОрганизацияЗаказчикНаименование,
	|	Документы.Организация.КодПоОКПО КАК ОрганизацияЗаказчикКодПоОКПО,
	|	Документы.Организация.Префикс КАК Префикс,
	|	Документы.Склад КАК Склад,
	#Вставка
	// 4D:ТКАнрекс, МаксимК,  
	// Доработка и ошибка. Оплата 50%. Печатная форма Списание на расходы,№ANKT-6
	// {
	|	Документы.Комментарий КАК Комментарий,
	// }
	// 4D
	#КонецВставки
	|	Документы.Подразделение КАК Подразделение,
	|	ПРЕДСТАВЛЕНИЕССЫЛКИ(Документы.Подразделение) КАК ПодразделениеНаименование,
	|	Документы.Склад.ТекущийОтветственный КАК КладовщикОтправитель,
	|	Документы.Склад.ТекущаяДолжностьОтветственного КАК ДолжностьКладовщикаОтправителя,
	|	Документы.ВидЦены КАК УчетныйВидЦены,
	|	Документы.ВидЦены.ВалютаЦены КАК УчетныйВидЦеныВалютаЦены,
	|	Документы.Комиссия КАК Комиссия
	|ИЗ
	|	Документ.ВнутреннееПотреблениеТоваров КАК Документы
	|ГДЕ
	|	Документы.Ссылка В(&МассивДокументов)
	|	И Документы.Ссылка.ХозяйственнаяОперация = ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.СписаниеТоваровПоТребованию)
	|
	|УПОРЯДОЧИТЬ ПО
	|	ДатаДокумента,
	|	Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	Товары.Ссылка КАК Ссылка,
	|	Товары.Номенклатура КАК Номенклатура,
	|	Товары.Номенклатура.НаименованиеПолное КАК НоменклатураНаименование,
	|	Товары.Характеристика.НаименованиеПолное КАК ХарактеристикаНаименование,
	|	Товары.Упаковка КАК Упаковка,
	|	Товары.Количество КАК Количество,
	|	ВЫРАЗИТЬ(ЕСТЬNULL(Цены.Цена, 0) / ЕСТЬNULL(&ТекстЗапросаКоэффициентУпаковки, 1) * Товары.Количество КАК ЧИСЛО(15, 2)) КАК СтоимостьВсего,
	|	ВЫРАЗИТЬ(ЕСТЬNULL(Цены.Цена, 0) / ЕСТЬNULL(&ТекстЗапросаКоэффициентУпаковки, 1) КАК ЧИСЛО(15, 2)) КАК СтоимостьЕдиницы,
	|	Товары.НомерСтроки КАК НомерСтроки
	|ИЗ
	|	Товары КАК Товары
	|		ЛЕВОЕ СОЕДИНЕНИЕ Цены КАК Цены
	|		ПО Товары.Ссылка = Цены.Ссылка
	|			И Товары.НомерСтроки = Цены.НомерСтроки
	|
	|УПОРЯДОЧИТЬ ПО
	|	Ссылка,
	|	НомерСтроки
	|ИТОГИ ПО
	|	Ссылка");

	Запрос.Текст = СтрЗаменить(Запрос.Текст,
	"&ТекстЗапросаКоэффициентУпаковки",
	Справочники.УпаковкиЕдиницыИзмерения.ТекстЗапросаКоэффициентаУпаковки(
	"Цены.Упаковка",
	"Товары.Номенклатура"));

	Запрос.УстановитьПараметр("МассивДокументов", МассивДокументов);

	РезультатПакетаЗапросов = Запрос.ВыполнитьПакет();
	// Пакет запросов:
	// 		[0] - Временная таблица по табличной части документа
	// 		[1] - Временная таблица по ценам номенклатуры табличной части
	// 		[2] - Выборка по шапкам документов
	// 		[3] - Выборка по табличным частям документов.

	Возврат Новый Структура(
	"РезультатПоШапке, РезультатПоТабличнойЧасти",
	РезультатПакетаЗапросов[2],
	РезультатПакетаЗапросов[3]);

КонецФункции

#КонецОбласти
