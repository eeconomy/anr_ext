﻿
#Область ПрограммныйИнтерфейс

#Область ЗаполнениеОбработчиковФормы

&После("ПриСозданииНаСервере")
Процедура ТК_ПриСозданииНаСервере(Форма, Отказ, СтандартнаяОбработка)
	
	// 4D:ТКАнрэкс, АндрейБ, 23.12.2020 
	// Перенос доработок с 2.2 в 2.4 с расширением, №27697
	// {
	Чд_УправлениеФормамиВызовМенеджера.ПриСозданииНаСервере(Форма, Отказ, СтандартнаяОбработка);
	// }
	// 4D
	
КонецПроцедуры

&После("ПриЧтенииНаСервере")
Процедура ТК_ПриЧтенииНаСервере(Форма, ТекущийОбъект)
	
	// 4D:ТКАнрэкс, АндрейБ, 23.12.2020 
	// Перенос доработок с 2.2 в 2.4 с расширением, №27697
	// {
	Чд_УправлениеФормамиВызовМенеджера.ПриЧтенииНаСервере(Форма, ТекущийОбъект);
	// }
	// 4D
	
КонецПроцедуры

&После("ПередЗаписьюНаСервере")
Процедура ТК_ПередЗаписьюНаСервере(Форма, Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	// 4D:ТКАнрэкс, АндрейБ, 23.12.2020 
	// Перенос доработок с 2.2 в 2.4 с расширением, №27697
	// {
	Чд_УправлениеФормамиВызовМенеджера.ПередЗаписьюНаСервере(Форма, Отказ, ТекущийОбъект, ПараметрыЗаписи);
	// }
	// 4D
	
КонецПроцедуры

&После("ПослеЗаписиНаСервере")
Процедура ТК_ПослеЗаписиНаСервере(Форма, ТекущийОбъект, ПараметрыЗаписи)
	
	// 4D:ТКАнрэкс, АндрейБ, 23.12.2020 
	// Перенос доработок с 2.2 в 2.4 с расширением, №27697
	// {
	Чд_УправлениеФормамиВызовМенеджера.ПослеЗаписиНаСервере(Форма, ТекущийОбъект, ПараметрыЗаписи);
	// }
	// 4D
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти
