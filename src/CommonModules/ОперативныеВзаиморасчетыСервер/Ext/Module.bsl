﻿&ИзменениеИКонтроль("РассчитатьПоИзменениям")
Процедура ТК_РассчитатьПоИзменениям(МенеджерВременныхТаблиц, ЭтоРасчетыСКлиентами, Регистратор, ДополнительныеСвойства)

	ТаблицаИзменений = ТаблицаИзмененийДляПересчета(МенеджерВременныхТаблиц, ЭтоРасчетыСКлиентами, Регистратор);

	Для Каждого СтрокаИзменений Из ТаблицаИзменений Цикл
		ОсновныеПараметры = СтруктураПараметровЗаполненияВзаиморасчетов();

		ЗаполнитьЗначенияСвойств(ОсновныеПараметры, СтрокаИзменений);
		Если СтрокаИзменений.ПорядокФакт <> "" Тогда
			ОсновныеПараметры.ПорядокФакт = Лев(СтрокаИзменений.ПорядокФакт, 8) + "000000000";
			#Удаление
			ОсновныеПараметры.ПорядокПлан = Лев(СтрокаИзменений.ПорядокПлан, 8) + "000000000";
			#КонецУдаления
			#Вставка
			// 4D:"Анрэкс", МаксимК, 26.07.2022 
			//  Уценка товаров на основании ПТИУ, №ANKT-184
			// {			
			ОсновныеПараметры.ПорядокПлан = Лев(СтрокаИзменений.ПорядокПлан, 8) + "923595900";
			// }
			// 4D
			#КонецВставки
		КонецЕсли;
		ОсновныеПараметры.ЭтоРасчетыСКлиентами = ЭтоРасчетыСКлиентами;
		ОсновныеПараметры.Регистратор = Регистратор;
		ОсновныеПараметры.ДополнительныеСвойстваПроведения = ДополнительныеСвойства;

		ЗаполнитьОперативныеВзаиморасчеты(ОсновныеПараметры);
	КонецЦикла;

КонецПроцедуры
  
&ИзменениеИКонтроль("ПереоценитьДолг")
Процедура ТК_ПереоценитьДолг(ГлобальныеПеременные, ТаблицаРасчетовПоДокументам, СтрокаОтгрузки, СтрокаОплаты, ПереоценкаНаДатуОплаты)

	#Удаление
	// 4D:ERP для Беларуси Екатерина, 09.03.2022 15:04:59 
	// №32381
	// {
	Если ГлобальныеПеременные.ЕжедневнаяПереоценка Тогда
		ДатаКэш = НачалоДня(СтрокаОтгрузки.ДатаПереоценки);
	Иначе
		ДатаКэш = НачалоДня(КонецМесяца(СтрокаОтгрузки.ДатаПереоценки));
	КонецЕсли;

	ОкончательнаяДатаПереоценки = ?(СтрокаОплаты <> Неопределено, НачалоДня(СтрокаОплаты.Период), ТекущаяДатаСеанса());

	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("НачалоПереоценки", ДатаКэш);
	Запрос.УстановитьПараметр("КонецПереоценки", ОкончательнаяДатаПереоценки);
	Запрос.УстановитьПараметр("АналитикаУчетаПоПартнерам" , ГлобальныеПеременные.АналитикаУчетаПоПартнерам);
	Запрос.УстановитьПараметр("Регистратор", ГлобальныеПеременные.Регистратор);
	Если ГлобальныеПеременные.ДополнительныеСвойстваПроведения = Неопределено Тогда 
		Запрос.УстановитьПараметр("ЭтоОтменаПроведения", Ложь);
		Запрос.УстановитьПараметр("ДатаРегистратора", Дата(1, 1, 1));
	Иначе
		Запрос.УстановитьПараметр("ЭтоОтменаПроведения", ?(ГлобальныеПеременные.ДополнительныеСвойстваПроведения.Свойство("РежимЗаписи") И ГлобальныеПеременные.ДополнительныеСвойстваПроведения.РежимЗаписи = РежимЗаписиДокумента.ОтменаПроведения, Истина, Ложь));
		Запрос.УстановитьПараметр("ДатаРегистратора", ?(ГлобальныеПеременные.ДополнительныеСвойстваПроведения.Свойство("ДатаРегистратора"), ГлобальныеПеременные.ДополнительныеСвойстваПроведения.ДатаРегистратора, Дата(1, 1, 1)));
	КонецЕсли;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ВложенныйЗапрос.Период КАК Период,
	|	ВложенныйЗапрос.Регистратор КАК Регистратор
	|ИЗ
	|	(ВЫБРАТЬ
	|		РасчетыСПоставщикамиПоСрокам.АналитикаУчетаПоПартнерам КАК АналитикаУчетаПоПартнерам,
	|		НАЧАЛОПЕРИОДА(РасчетыСПоставщикамиПоСрокам.Период, ДЕНЬ) КАК Период,
	|		РасчетыСПоставщикамиПоСрокам.Регистратор КАК Регистратор
	|	ИЗ
	|		РегистрНакопления.РасчетыСПоставщикамиПоСрокам КАК РасчетыСПоставщикамиПоСрокам
	|	ГДЕ
	|		РасчетыСПоставщикамиПоСрокам.АналитикаУчетаПоПартнерам В(&АналитикаУчетаПоПартнерам)
	|		И РасчетыСПоставщикамиПоСрокам.Период МЕЖДУ &НачалоПереоценки И &КонецПереоценки
	|		И РасчетыСПоставщикамиПоСрокам.Долг <> 0
	|		И ВЫБОР
	|				КОГДА &ЭтоОтменаПроведения
	|					ТОГДА РасчетыСПоставщикамиПоСрокам.ДокументРегистратор <> &Регистратор
	|				ИНАЧЕ ИСТИНА
	|			КОНЕЦ
	|	
	|	ОБЪЕДИНИТЬ ВСЕ
	|	
	|	ВЫБРАТЬ
	|		РасчетыСКлиентамиПоСрокам.АналитикаУчетаПоПартнерам,
	|		НАЧАЛОПЕРИОДА(РасчетыСКлиентамиПоСрокам.Период, ДЕНЬ),
	|		РасчетыСКлиентамиПоСрокам.Регистратор
	|	ИЗ
	|		РегистрНакопления.РасчетыСКлиентамиПоСрокам КАК РасчетыСКлиентамиПоСрокам
	|	ГДЕ
	|		РасчетыСКлиентамиПоСрокам.Долг <> 0
	|		И РасчетыСКлиентамиПоСрокам.АналитикаУчетаПоПартнерам В(&АналитикаУчетаПоПартнерам)
	|		И РасчетыСКлиентамиПоСрокам.Период МЕЖДУ &НачалоПереоценки И &КонецПереоценки
	|		И ВЫБОР
	|				КОГДА &ЭтоОтменаПроведения
	|					ТОГДА РасчетыСКлиентамиПоСрокам.ДокументРегистратор <> &Регистратор
	|				ИНАЧЕ ИСТИНА
	|			КОНЕЦ
	|	
	|	ОБЪЕДИНИТЬ ВСЕ
	|	
	|	ВЫБРАТЬ
	|		&АналитикаУчетаПоПартнерам,
	|		ВЫБОР
	|			КОГДА &ЭтоОтменаПроведения
	|				ТОГДА ДАТАВРЕМЯ(1, 1, 1)
	|			ИНАЧЕ НАЧАЛОПЕРИОДА(&ДатаРегистратора, ДЕНЬ)
	|		КОНЕЦ,
	|		NULL) КАК ВложенныйЗапрос
	|
	|СГРУППИРОВАТЬ ПО
	|	ВложенныйЗапрос.Период,
	|	ВложенныйЗапрос.Регистратор
	|
	|УПОРЯДОЧИТЬ ПО
	|	Период";   

	ТЗДаты = Запрос.Выполнить().Выгрузить();

	Пока ДатаКэш < ОкончательнаяДатаПереоценки Цикл

		Отбор = Новый Структура;
		Отбор.Вставить("Период", ДатаКэш);
		НашлиДату = ТЗДаты.НайтиСтроки(Отбор);

		//Отгрузку могли уже переоценить до даты предыдущей оплаты.
		Если ДатаКэш > СтрокаОтгрузки.ДатаПереоценки 
			И (НачалоДня(КонецМесяца(ДатаКэш)) = ДатаКэш ИЛИ НашлиДату.Количество() > 0) Тогда

			СтрокаКурса = ГлобальныеПеременные.ТаблицаКурсовВалют.Найти(ДатаКэш, "Дата");

			// 4D:ERP для Беларуси, ВладимирР, 26.08.2020 9:03:57 
			// Ввод начальных остатков задолженности клиентов, №26418
			// {
			Если СтрокаКурса = Неопределено Тогда
				СтрокаКурса = Новый Структура;
				СтрокаКурса.Вставить("КурсРегл", 1);
				СтрокаКурса.Вставить("КурсУпр", 1);
			КонецЕсли;
			// }
			// 4D

			СуммаПереоценкиРегл = Окр(СтрокаОтгрузки.Сумма * СтрокаКурса.КурсРегл, 2) - СтрокаОтгрузки.СуммаРеглДляПереоценки;
			СуммаПереоценкиУпр = Окр(СтрокаОтгрузки.Сумма * СтрокаКурса.КурсУпр, 2) - СтрокаОтгрузки.СуммаУпрДляПереоценки;

			Если СуммаПереоценкиРегл <> 0 ИЛИ СуммаПереоценкиУпр <> 0 Тогда
				ДобавитьЗаписьПереоценки(ГлобальныеПеременные, ТаблицаРасчетовПоДокументам, КонецДня(ДатаКэш), СтрокаОтгрузки, "Долг", СуммаПереоценкиРегл, СуммаПереоценкиУпр);
			КонецЕсли;

			СтрокаОтгрузки.СуммаРеглДляПереоценки      = СтрокаОтгрузки.СуммаРеглДляПереоценки + СуммаПереоценкиРегл;
			СтрокаОтгрузки.СуммаУпрДляПереоценки       = СтрокаОтгрузки.СуммаУпрДляПереоценки + СуммаПереоценкиУпр;

			СтрокаОтгрузки.ДатаПереоценки = ДатаКэш;

		КонецЕсли;

		Если ГлобальныеПеременные.ЕжедневнаяПереоценка Тогда
			ДатаКэш = ДатаКэш + 86400;
		Иначе
			ДатаКэш = НачалоДня(КонецМесяца(КонецМесяца(ДатаКэш)+1));
		КонецЕсли;

	КонецЦикла;

	//Если ГлобальныеПеременные.ЕжедневнаяПереоценка Тогда
	//	ДатаКэш = НачалоДня(СтрокаОтгрузки.ДатаПереоценки);
	//Иначе
	//	ДатаКэш = НачалоДня(КонецМесяца(СтрокаОтгрузки.ДатаПереоценки));
	//КонецЕсли;
	//
	//ОкончательнаяДатаПереоценки = ?(СтрокаОплаты <> Неопределено, НачалоДня(СтрокаОплаты.Период), ТекущаяДатаСеанса());
	//
	//Пока ДатаКэш < ОкончательнаяДатаПереоценки Цикл
	//	
	//	//Отгрузку могли уже переоценить до даты предыдущей оплаты.
	//	Если ДатаКэш > СтрокаОтгрузки.ДатаПереоценки Тогда
	//		СтрокаКурса = ГлобальныеПеременные.ТаблицаКурсовВалют.Найти(ДатаКэш, "Дата");
	//		
	//		// 4D:ERP для Беларуси, ВладимирР, 26.08.2020 9:03:57 
	//		// Ввод начальных остатков задолженности клиентов, №26418
	//		// {
	//		Если СтрокаКурса = Неопределено Тогда
	//			СтрокаКурса = Новый Структура;
	//			СтрокаКурса.Вставить("КурсРегл", 1);
	//			СтрокаКурса.Вставить("КурсУпр", 1);
	//		КонецЕсли;
	//		// }
	//		// 4D
	//		
	//		СуммаПереоценкиРегл = Окр(СтрокаОтгрузки.Сумма * СтрокаКурса.КурсРегл, 2) - СтрокаОтгрузки.СуммаРеглДляПереоценки;
	//		СуммаПереоценкиУпр = Окр(СтрокаОтгрузки.Сумма * СтрокаКурса.КурсУпр, 2) - СтрокаОтгрузки.СуммаУпрДляПереоценки;
	//		
	//		Если СуммаПереоценкиРегл <> 0 ИЛИ СуммаПереоценкиУпр <> 0 Тогда
	//			ДобавитьЗаписьПереоценки(ГлобальныеПеременные, ТаблицаРасчетовПоДокументам, КонецДня(ДатаКэш), СтрокаОтгрузки, "Долг", СуммаПереоценкиРегл, СуммаПереоценкиУпр);
	//		КонецЕсли;
	//		
	//		СтрокаОтгрузки.СуммаРеглДляПереоценки      = СтрокаОтгрузки.СуммаРеглДляПереоценки + СуммаПереоценкиРегл;
	//		СтрокаОтгрузки.СуммаУпрДляПереоценки       = СтрокаОтгрузки.СуммаУпрДляПереоценки + СуммаПереоценкиУпр;
	//		
	//		СтрокаОтгрузки.ДатаПереоценки = ДатаКэш;
	//		
	//	КонецЕсли;
	//	
	//	Если ГлобальныеПеременные.ЕжедневнаяПереоценка Тогда
	//		ДатаКэш = ДатаКэш + 86400;
	//	Иначе
	//		ДатаКэш = НачалоДня(КонецМесяца(КонецМесяца(ДатаКэш)+1));
	//	КонецЕсли;
	//	
	//КонецЦикла;
	// }
	// 4D

	//Требуется переоценка на дату оплаты.
	Если СтрокаОплаты <> Неопределено И НачалоДня(СтрокаОплаты.Период) > СтрокаОтгрузки.ДатаПереоценки И ПереоценкаНаДатуОплаты Тогда

		СтрокаКурса = ГлобальныеПеременные.ТаблицаКурсовВалют.Найти(НачалоДня(СтрокаОплаты.Период), "Дата");

		// 4D:ERP для Беларуси, ВладимирР, 26.08.2020 9:03:57 
		// Ввод начальных остатков задолженности клиентов, №26418
		// {
		Если СтрокаКурса = Неопределено Тогда
			СтрокаКурса = Новый Структура;
			СтрокаКурса.Вставить("КурсРегл", 1);
			СтрокаКурса.Вставить("КурсУпр", 1);
		КонецЕсли;
		// }
		// 4D

		//Долг всегда переоценивается на дату оплаты.
		СуммаПереоценкиРегл = Окр(СтрокаОтгрузки.Сумма * СтрокаКурса.КурсРегл, 2) - СтрокаОтгрузки.СуммаРеглДляПереоценки;
		СуммаПереоценкиУпр = Окр(СтрокаОтгрузки.Сумма * СтрокаКурса.КурсУпр, 2) - СтрокаОтгрузки.СуммаУпрДляПереоценки;

		Если СуммаПереоценкиРегл <> 0 ИЛИ СуммаПереоценкиУпр <> 0 Тогда
			ДобавитьЗаписьПереоценки(ГлобальныеПеременные, ТаблицаРасчетовПоДокументам, НачалоДня(СтрокаОплаты.Период), СтрокаОтгрузки, "Долг", СуммаПереоценкиРегл, СуммаПереоценкиУпр);
		КонецЕсли;

		СтрокаОтгрузки.СуммаРеглДляПереоценки      = СтрокаОтгрузки.СуммаРеглДляПереоценки + СуммаПереоценкиРегл;
		СтрокаОтгрузки.СуммаУпрДляПереоценки       = СтрокаОтгрузки.СуммаУпрДляПереоценки + СуммаПереоценкиУпр;

		СтрокаОтгрузки.ДатаПереоценки = НачалоДня(СтрокаОплаты.Период);

	КонецЕсли;
	#КонецУдаления
	#Вставка
	// 4D:"Анрэкс", МаксимК, 25.04.2022
	//  Исправить ошибку при закрытии месяца, №ANKT-154
	// {	
	Если ГлобальныеПеременные.ЕжедневнаяПереоценка Тогда
		ДатаКэш = НачалоДня(СтрокаОтгрузки.ДатаПереоценки);
	Иначе
		ДатаКэш = НачалоДня(КонецМесяца(СтрокаОтгрузки.ДатаПереоценки));
	КонецЕсли;
	
	ОкончательнаяДатаПереоценки = ?(СтрокаОплаты <> Неопределено, НачалоДня(СтрокаОплаты.Период), ТекущаяДатаСеанса());
	
	Пока ДатаКэш < ОкончательнаяДатаПереоценки Цикл
		
		//Отгрузку могли уже переоценить до даты предыдущей оплаты.
		Если ДатаКэш > СтрокаОтгрузки.ДатаПереоценки Тогда
			СтрокаКурса = ГлобальныеПеременные.ТаблицаКурсовВалют.Найти(ДатаКэш, "Дата");
			
			// 4D:ERP для Беларуси, ВладимирР, 26.08.2020 9:03:57 
			// Ввод начальных остатков задолженности клиентов, №26418
			// {
			Если СтрокаКурса = Неопределено Тогда
				СтрокаКурса = Новый Структура;
				СтрокаКурса.Вставить("КурсРегл", 1);
				СтрокаКурса.Вставить("КурсУпр", 1);
			КонецЕсли;
			// }
			// 4D
			
			СуммаПереоценкиРегл = Окр(СтрокаОтгрузки.Сумма * СтрокаКурса.КурсРегл, 2) - СтрокаОтгрузки.СуммаРеглДляПереоценки;
			СуммаПереоценкиУпр = Окр(СтрокаОтгрузки.Сумма * СтрокаКурса.КурсУпр, 2) - СтрокаОтгрузки.СуммаУпрДляПереоценки;
			
			Если СуммаПереоценкиРегл <> 0 ИЛИ СуммаПереоценкиУпр <> 0 Тогда
				ДобавитьЗаписьПереоценки(ГлобальныеПеременные, ТаблицаРасчетовПоДокументам, КонецДня(ДатаКэш), СтрокаОтгрузки, "Долг", СуммаПереоценкиРегл, СуммаПереоценкиУпр);
			КонецЕсли;
			
			СтрокаОтгрузки.СуммаРеглДляПереоценки      = СтрокаОтгрузки.СуммаРеглДляПереоценки + СуммаПереоценкиРегл;
			СтрокаОтгрузки.СуммаУпрДляПереоценки       = СтрокаОтгрузки.СуммаУпрДляПереоценки + СуммаПереоценкиУпр;
			
			СтрокаОтгрузки.ДатаПереоценки = ДатаКэш;
			
		КонецЕсли;
		
		Если ГлобальныеПеременные.ЕжедневнаяПереоценка Тогда
			ДатаКэш = ДатаКэш + 86400;
		Иначе
			ДатаКэш = НачалоДня(КонецМесяца(КонецМесяца(ДатаКэш)+1));
		КонецЕсли;
		
	КонецЦикла;
	
	//Требуется переоценка на дату оплаты.
	Если СтрокаОплаты <> Неопределено И НачалоДня(СтрокаОплаты.Период) > СтрокаОтгрузки.ДатаПереоценки И ПереоценкаНаДатуОплаты Тогда
		
		СтрокаКурса = ГлобальныеПеременные.ТаблицаКурсовВалют.Найти(НачалоДня(СтрокаОплаты.Период), "Дата");
		
		// 4D:ERP для Беларуси, ВладимирР, 26.08.2020 9:03:57 
		// Ввод начальных остатков задолженности клиентов, №26418
		// {
		Если СтрокаКурса = Неопределено Тогда
			СтрокаКурса = Новый Структура;
			СтрокаКурса.Вставить("КурсРегл", 1);
			СтрокаКурса.Вставить("КурсУпр", 1);
		КонецЕсли;
		// }
		// 4D
		
		//Долг всегда переоценивается на дату оплаты.
		СуммаПереоценкиРегл = Окр(СтрокаОтгрузки.Сумма * СтрокаКурса.КурсРегл, 2) - СтрокаОтгрузки.СуммаРеглДляПереоценки;
		СуммаПереоценкиУпр = Окр(СтрокаОтгрузки.Сумма * СтрокаКурса.КурсУпр, 2) - СтрокаОтгрузки.СуммаУпрДляПереоценки;
		
		Если СуммаПереоценкиРегл <> 0 ИЛИ СуммаПереоценкиУпр <> 0 Тогда
			ДобавитьЗаписьПереоценки(ГлобальныеПеременные, ТаблицаРасчетовПоДокументам, НачалоДня(СтрокаОплаты.Период), СтрокаОтгрузки, "Долг", СуммаПереоценкиРегл, СуммаПереоценкиУпр);
		КонецЕсли;
		
		СтрокаОтгрузки.СуммаРеглДляПереоценки      = СтрокаОтгрузки.СуммаРеглДляПереоценки + СуммаПереоценкиРегл;
		СтрокаОтгрузки.СуммаУпрДляПереоценки       = СтрокаОтгрузки.СуммаУпрДляПереоценки + СуммаПереоценкиУпр;
		
		СтрокаОтгрузки.ДатаПереоценки = НачалоДня(СтрокаОплаты.Период);
		
	КонецЕсли;
	// }
	// 4D
	#КонецВставки

КонецПроцедуры

