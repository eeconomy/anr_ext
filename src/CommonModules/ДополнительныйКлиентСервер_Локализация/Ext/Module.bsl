﻿#Область Локализация

&ИзменениеИКонтроль("ПересчитатьСуммуСУчетомРучнойСкидкиВСтрокеТЧ")
Процедура ТК_ПересчитатьСуммуСУчетомРучнойСкидкиВСтрокеТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения)

	СтруктураПараметровДействия = Неопределено;

	Если СтруктураДействий.Свойство("ПересчитатьСуммуСУчетомРучнойСкидки", СтруктураПараметровДействия) Тогда

		Очищать = Неопределено;
		ПересчитыватьСуммуРучнойСкидки = Неопределено;
		ИмяКоличества = Неопределено;

		Если СтруктураПараметровДействия <> Неопределено Тогда

			СтруктураПараметровДействия.Свойство("Очищать", Очищать);
			СтруктураПараметровДействия.Свойство("ПересчитыватьСуммуРучнойСкидки", ПересчитыватьСуммуРучнойСкидки);
			СтруктураПараметровДействия.Свойство("ИмяКоличества", ИмяКоличества);

			Если НЕ ЗначениеЗаполнено(ИмяКоличества) Тогда
				Попытка
					ИмяКоличества = ?(ТекущаяСтрока.Свойство("КоличествоУпаковок"), "КоличествоУпаковок", "Количество");
				Исключение
					ИмяКоличества = "Количество";
				КонецПопытки;
			КонецЕсли; 

			Если Очищать = Истина Тогда		
				ТекущаяСтрока.СуммаРучнойСкидки = 0;
				ТекущаяСтрока.ПроцентРучнойСкидки = 0;
				ТекущаяСтрока.ЦенаСоСкидкой = 0;
			КонецЕсли;

		КонецЕсли;

		Если ПересчитыватьСуммуРучнойСкидки = Ложь Тогда

		Иначе
			ТекущаяСтрокаСкидка = 0;

			Если НЕ ЗначениеЗаполнено(ИмяКоличества) Тогда
				Попытка
					ИмяКоличества = ?(ТекущаяСтрока.Свойство("КоличествоУпаковок"), "КоличествоУпаковок", "Количество");
				Исключение
					ИмяКоличества = "Количество";
				КонецПопытки;
			КонецЕсли;

			Если ТипЗнч(ТекущаяСтрока) = Тип("Структура") Тогда	
				Если ТекущаяСтрока.Свойство("Скидка") И ТекущаяСтрока.Скидка <> Неопределено Тогда
					ТекущаяСтрокаСкидка = ТекущаяСтрока.Скидка;
				КонецЕсли;	
			Иначе
				Попытка
					Если ТекущаяСтрока.Свойство("Скидка") И ТекущаяСтрока.Скидка <> 0 ТОгда
						ТекущаяСтрокаСкидка = ТекущаяСтрока.Скидка;	
					КонецЕсли;
				Исключение
				КонецПопытки;
			КонецЕсли;

			Если ТекущаяСтрокаСкидка = 0 Тогда
				ТекущаяСтрока.СуммаРучнойСкидки = Окр(ТекущаяСтрока[ИмяКоличества] * ТекущаяСтрока.Цена * ТекущаяСтрока.ПроцентРучнойСкидки / 100, 2);
			Иначе
				ТекущаяСтрока.СуммаРучнойСкидки = ТекущаяСтрокаСкидка * ТекущаяСтрока[ИмяКоличества];	
			КонецЕсли;
		КонецЕсли;

		Попытка
			Если ТекущаяСтрока.Свойство("ЦенаСоСкидкой") И ТекущаяСтрока.ЦенаСоСкидкой <> 0 Тогда
				ТекущаяСтрока.Сумма = Окр(ТекущаяСтрока.ЦенаСоСкидкой * ТекущаяСтрока[ИмяКоличества], 2); 

				Если ТекущаяСтрока.ПроцентРучнойСкидки <> 0 И ТекущаяСтрока.ПроцентАвтоматическойСкидки = 0 Тогда
					ТекущаяСтрока.СуммаРучнойСкидки = Окр(ТекущаяСтрока[ИмяКоличества] * (ТекущаяСтрока.Цена - ТекущаяСтрока.ЦенаСоСкидкой), 2);	
				КонецЕсли; 

			#Удаление	
			ИначеЕсли ТекущаяСтрока.Свойство("Скидка") И ТекущаяСтрока.Скидка <> 0 И ТекущаяСтрока.Скидка <> Неопределено Тогда
			#КонецУдаления
			#Вставка
			// 4D:"Анрэкс", ВикторК, 06.01.2021
			// Задача № 27697 - Перенос доработок с 2.2 в 2.4 с расширением
			// {перенос доработки по задаче №23117 
			ИначеЕсли ТекущаяСтрока.Свойство("Скидка") И ТекущаяСтрока.Скидка <> 0 И ТекущаяСтрока.Свойство("Скидка" = Неопределено)  Тогда
			// 
			// }4D 
			#КонецВставки
				ТекущаяСтрока.ЦенаСоСкидкой = ТекущаяСтрока.Цена - ТекущаяСтрока.Скидка;

			ИначеЕсли ТекущаяСтрока.СуммаРучнойСкидки <> 0 Тогда 
				//ТекущаяСтрока.ЦенаСоСкидкой = ТекущаяСтрока.Цена - Окр(ТекущаяСтрока.ПроцентРучнойСкидки * ТекущаяСтрока.Цена / 100, 2);
				Если ТекущаяСтрока[ИмяКоличества] <> 0 Тогда
					ТекущаяСтрока.ЦенаСоСкидкой = 
					Окр(ТекущаяСтрока.Цена - ТекущаяСтрока.ПроцентРучнойСкидки * ТекущаяСтрока.Цена / 100 - ТекущаяСтрока.СуммаАвтоматическойСкидки / ТекущаяСтрока[ИмяКоличества], 2);
				КонецЕсли; 

				Если ТекущаяСтрока.Сумма <> ТекущаяСтрока.ЦенаСоСкидкой * ТекущаяСтрока[ИмяКоличества] Тогда
					ТекущаяСтрока.Сумма = ТекущаяСтрока.Сумма - Окр(ТекущаяСтрока.ПроцентРучнойСкидки * ТекущаяСтрока.Цена / 100, 2) * ТекущаяСтрока[ИмяКоличества];
				КонецЕсли; 

				Если ТекущаяСтрока.СуммаАвтоматическойСкидки = 0 Тогда
					ТекущаяСтрока.СуммаРучнойСкидки = Окр(ТекущаяСтрока[ИмяКоличества] * (ТекущаяСтрока.Цена - ТекущаяСтрока.ЦенаСоСкидкой), 2);
				КонецЕсли; 

			Иначе
				ТекущаяСтрока.Сумма = Окр(ТекущаяСтрока.Цена * ТекущаяСтрока[ИмяКоличества], 2);
			КонецЕсли;

		Исключение
			Попытка
				#Удаление
				Если ТекущаяСтрока.Свойство("ЦенаСоСкидкой") И ТекущаяСтрока.ЦенаСоСкидкой <> 0 И ТекущаяСтрока.ЦенаСоСкидкой <> Неопределено Тогда
				#КонецУдаления
				#Вставка
				// 4D:Анрэкс, ВикторК, 11.01.2021
				// Задача № 27697 - Перенос доработок с 2.2 в 2.4 с расширением
				// {перенос доработки по задаче №23117
				Если ТекущаяСтрока.ЦенаСоСкидкой <> 0 И ТекущаяСтрока.ЦенаСоСкидкой <> Неопределено Тогда
				#КонецВставки
					ТекущаяСтрока.Сумма = Окр(ТекущаяСтрока.ЦенаСоСкидкой * ТекущаяСтрока[ИмяКоличества], 2); 

				ИначеЕсли ТекущаяСтрока.СуммаРучнойСкидки <> 0 Тогда	
					ТекущаяСтрока.Сумма = ТекущаяСтрока.Сумма - ТекущаяСтрока.СуммаРучнойСкидки;

				Иначе
					ТекущаяСтрока.Сумма = Окр(ТекущаяСтрока.Цена * ТекущаяСтрока[ИмяКоличества], 2);
				КонецЕсли;

			Исключение
				Если ТекущаяСтрока.ЦенаСоСкидкой <> 0 И ТекущаяСтрока.ЦенаСоСкидкой <> Неопределено Тогда
					ТекущаяСтрока.Сумма = Окр(ТекущаяСтрока.ЦенаСоСкидкой * ТекущаяСтрока[ИмяКоличества], 2); 

				ИначеЕсли ТекущаяСтрока.СуммаРучнойСкидки <> 0 Тогда	
					ТекущаяСтрока.Сумма = ТекущаяСтрока.Сумма - ТекущаяСтрока.СуммаРучнойСкидки;

				Иначе
					ТекущаяСтрока.Сумма = Окр(ТекущаяСтрока.Цена * ТекущаяСтрока[ИмяКоличества], 2);
				КонецЕсли;

			КонецПопытки;

		КонецПопытки;

	КонецЕсли;

КонецПроцедуры

#КонецОбласти