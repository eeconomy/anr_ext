﻿
&ИзменениеИКонтроль("СформироватьПечатнуюФормуАктСписанияИспорченныхБСО")
Функция ТК_СформироватьПечатнуюФормуАктСписанияИспорченныхБСО(МассивОбъектов, ОбъектыПечати, КомплектыПечати)
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("МассивОбъектов", МассивОбъектов);
	Запрос.Текст =
	"ВЫБРАТЬ
	|	БСО.Ссылка КАК Ссылка,
	|	БСО.Номер КАК НомерДокумента,
	|	БСО.Дата КАК Дата,
	|	БСО.Организация КАК Организация,
	|	БСО.Организация.НаименованиеПолное КАК ПредставлениеОрганизации,
	|	БСО.Ответственный КАК Ответственный,
	|	БСО.Подразделение КАК Подразделение,
	|	ПРЕДСТАВЛЕНИЕ(БСО.Подразделение) КАК ПодразделениеПредставление,
	#Вставка
	// 4D:ТКАнрекс, МаксимК,  
	// Комиссии для ПФ, №29014
	// {
	|	БСО.Чд_КомиссияДляПечати КАК Комиссия,
	// } 4D
	#КонецВставки
	|	БСО.Склад КАК Склад,
	|	БСО.Склад.ТекущаяДолжностьОтветственного КАК СкладДолжность,
	|	БСО.Склад.ТекущийОтветственный КАК СкладОтветственный
	|ИЗ
	|	Документ.ВнутреннееПотреблениеТоваров КАК БСО
	|ГДЕ
	|	БСО.Ссылка В(&МассивОбъектов)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	БСО.Ссылка КАК Ссылка,
	|	БСО.Номенклатура.Код КАК Код,
	|	БСО.Номенклатура КАК Номенклатура,
	|	БСО.СерияБланкаСтрогойОтчетности КАК Серия,
	|	БСО.НачальныйНомерБланкаСтрогойОтчетности КАК НачальныйНомер,
	|	БСО.КонечныйНомерБланкаСтрогойОтчетности КАК КонечныйНомер,
	|	БСО.Количество КАК Количество
	|ИЗ
	|	Документ.ВнутреннееПотреблениеТоваров.Товары КАК БСО
	|ГДЕ
	|	БСО.Ссылка В(&МассивОбъектов)
	|ИТОГИ
	|	СУММА(Количество)
	|ПО
	|	Ссылка,
	|	Номенклатура";
	
	Результат = Запрос.ВыполнитьПакет();
	Если Результат[0].Пустой() Или Результат[1].Пустой() Тогда
		Возврат Неопределено;
	КонецЕсли;
	
	ТабДокумент = Новый ТабличныйДокумент;
	ТабДокумент.ИмяПараметровПечати = "ПАРАМЕТРЫ_ПЕЧАТИ_ВнутреннееПотреблениеТоваров_ПФ_MXL_АктСписанияИспорченныхБСО";
	ТабДокумент.АвтоМасштаб = Истина;
	ТабДокумент.ОриентацияСтраницы = ОриентацияСтраницы.Портрет;
	ТабДокумент.ПолеСверху = 5;
	ТабДокумент.ПолеСлева = 10;
	ТабДокумент.ПолеСнизу = 5;
	ТабДокумент.ПолеСправа = 5;
	
	#Удаление
	Макет = УправлениеПечатью.МакетПечатнойФормы("Документ.ВнутреннееПотреблениеТоваров.ПФ_MXL_АктСписанияИспорченныхБСО");
	#КонецУдаления
	#Вставка
	// 4D:ТКАнрекс, МаксимК,  
	// Комиссии для ПФ, №29014
	// {
	Макет = УправлениеПечатью.МакетПечатнойФормы("Документ.ВнутреннееПотреблениеТоваров.Чд_ПФ_MXL_АктСписанияИспользованныхБСО");
	// } 4D
	#КонецВставки
	
	ПервыйДокумент = Истина;
	
	Шапка = Результат[0].Выбрать();
	ВыборкаСсылка = Результат[1].Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам, "Ссылка");
	
	Пока Шапка.Следующий() Цикл
		Если Не ПервыйДокумент Тогда
			ТабДокумент.ВывестиГоризонтальныйРазделительСтраниц();
		КонецЕсли;
		
		ПервыйДокумент = Ложь;
		НомерСтрокиНачало = ТабДокумент.ВысотаТаблицы + 1;
		
		ОтветственныеЛица = ОтветственныеЛицаБП.ОтветственныеЛица(Шапка.Организация, Шапка.Дата);
		
		ОблЗаголовок = Макет.ПолучитьОбласть("Заголовок");
		ОблЗаголовок1 = Макет.ПолучитьОбласть("Заголовок1");
		ОблЗаголовок2 = Макет.ПолучитьОбласть("Заголовок2");
		ОблСтрока = Макет.ПолучитьОбласть("Строка");
		ОблПодписи = Макет.ПолучитьОбласть("Подписи");
		ОблПустаяСтрока = Макет.ПолучитьОбласть("ПустаяСтрока");
		ОблКомиссия = Макет.ПолучитьОбласть("Комиссия");
		#Удаление
		ОблПодвалКомиссия = Макет.ПолучитьОбласть("ПодвалКомиссия");
		#КонецУдаления
		
		ВыборкаСсылка.Сбросить();
		СтуктураПоиска = Новый Структура("Ссылка", Шапка.Ссылка);
		Пока ВыборкаСсылка.НайтиСледующий(СтуктураПоиска) Цикл
			ВыборкаНоменклатура = ВыборкаСсылка.Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам, "Номенклатура");
			Пока ВыборкаНоменклатура.Следующий() Цикл
				ТабДокумент.ВывестиГоризонтальныйРазделительСтраниц();
				
				ОблЗаголовок.Параметры.Заполнить(Шапка);
				ОблЗаголовок.Параметры.ПредставлениеОрганизации = Шапка.ПредставлениеОрганизации;
				ОблЗаголовок.Параметры.Число = Формат(Шапка.Дата, "ДФ=дд");
				ОблЗаголовок.Параметры.Месяц = Формат(Шапка.Дата, "ДФ=ММММ");
				ОблЗаголовок.Параметры.Год = Формат(Шапка.Дата, "ДФ=гггг");
				ОблЗаголовок.Параметры.УполномоченноеЛицо = ОтветственныеЛица.РуководительДолжностьПредставление + " " + ОтветственныеЛица.РуководительПредставление;
				ОблЗаголовок.Параметры.НаименованиеБланка = ВыборкаНоменклатура.Номенклатура;
				ТабДокумент.Вывести(ОблЗаголовок);
				ТабДокумент.Вывести(ОблПустаяСтрока);
				
				ОблЗаголовок1.Параметры.Число = "";
				ОблЗаголовок1.Параметры.Месяц = "";
				ОблЗаголовок1.Параметры.Год = "";
				ТабДокумент.Вывести(ОблЗаголовок1);
				
				Выборка = ВыборкаНоменклатура.Выбрать(ОбходРезультатаЗапроса.Прямой);
				Пока Выборка.Следующий() Цикл
					ОблСтрока.Параметры.Заполнить(Выборка);
					ОблСтрока.Параметры.Серия = СокрЛП(Выборка.Серия);
					ТабДокумент.Вывести(ОблСтрока);
				КонецЦикла;
				
				ТабДокумент.Вывести(ОблПустаяСтрока);
				
				КоличествоПрописью = НРег(ЧислоПрописью(ВыборкаНоменклатура.Количество,, "бланк, бланка, бланков, м,,,,, 0"));
				ОблЗаголовок2.Параметры.Количество = ВыборкаНоменклатура.Количество;
				ОблЗаголовок2.Параметры.КоличествоПрописью = КоличествоПрописью;
				ТабДокумент.Вывести(ОблЗаголовок2);
				ТабДокумент.Вывести(ОблПустаяСтрока);
				#Удаление
				ТабДокумент.Вывести(ОблПодвалКомиссия);
				#КонецУдаления
				#Вставка
				// 4D:ТКАнрекс, МаксимК,  
				// Комиссии для ПФ, №29014
				// {
				СтруктураКомиссии = Справочники.КомиссииДляПечатныхФорм.ПолучитьСтруктуруКомиссии(Шапка.Комиссия);
				СтруктураКомиссии.Вставить("Ответственный", Шапка.СкладОтветственный);
				СтруктураКомиссии.Вставить("Должность", Шапка.СкладДолжность);
				ВывестиПодписи(ТабДокумент, СтруктураКомиссии, ОблПодписи);
				// } 4D
				#КонецВставки
			КонецЦикла;
		КонецЦикла;
		
		УправлениеПечатью.ЗадатьОбластьПечатиДокумента(ТабДокумент, НомерСтрокиНачало, ОбъектыПечати, Шапка.Ссылка);
	КонецЦикла;
	
	Возврат ТабДокумент;
КонецФункции



// 4D:ТКАнрекс, МаксимК,  
// Комиссии для ПФ, №29014
// {
Процедура ВывестиПодписи(ТабДокумент, СтруктураКомиссии, ОблПодписи, ОблПустаяСтрока = Неопределено)
	ПредседательКомиссии = СтруктураКомиссии.РезультатПоКомиссии.Председатель;
	ОблПодписи.Параметры.ПроизвольныйТекст = "Председатель:";
	Если ЗначениеЗаполнено(ПредседательКомиссии) Тогда
		ОблПодписи.Параметры.Должность = СтруктураКомиссии.РезультатПоКомиссии.Должность;
		ОблПодписи.Параметры.ФИО = ФизическиеЛицаУТ.ФамилияИнициалыФизЛица(ПредседательКомиссии);
	КонецЕсли;
	ТабДокумент.Вывести(ОблПодписи);
	
	Если ОблПустаяСтрока <> Неопределено Тогда
		ТабДокумент.Вывести(ОблПустаяСтрока);
	КонецЕсли;
	
	ВыводитьЗаголовок = Истина;
	ПроизвольныйТекст = "Члены комиссии:";
	Для Каждого ЧленКомиссии Из СтруктураКомиссии.ТаблицаИнвентаризационнаяКомиссия Цикл
		ОблПодписи.Параметры.ПроизвольныйТекст = ПроизвольныйТекст;
		ОблПодписи.Параметры.Должность = ЧленКомиссии.Должность;
		ОблПодписи.Параметры.ФИО = ФизическиеЛицаУТ.ФамилияИнициалыФизЛица(ЧленКомиссии.Сотрудник);
		ТабДокумент.Вывести(ОблПодписи);
		ПроизвольныйТекст = "";
		ВыводитьЗаголовок = Ложь;
	КонецЦикла;
	
	КоличествоЧленовКомиссии = СтруктураКомиссии.ТаблицаИнвентаризационнаяКомиссия.Количество();
	Если КоличествоЧленовКомиссии < 3 Тогда
		Для Итератор = (КоличествоЧленовКомиссии + 1) По 3 Цикл
			ОблПодписи.Параметры.ПроизвольныйТекст = ?(ВыводитьЗаголовок, "Члены комиссии:", "");
			ОблПодписи.Параметры.Должность = "";
			ОблПодписи.Параметры.ФИО = "";
			ТабДокумент.Вывести(ОблПодписи);
			ВыводитьЗаголовок = Ложь;
		КонецЦикла;
	КонецЕсли;
	
	Если ЗначениеЗаполнено(СтруктураКомиссии.Ответственный) Тогда
		ПроизвольныйТекст = "Материально ответственное лицо:";
		ОблПодписи.Параметры.ПроизвольныйТекст = ПроизвольныйТекст;
		ОблПодписи.Параметры.Должность = СтруктураКомиссии.Должность;
		ОблПодписи.Параметры.ФИО = ФизическиеЛицаУТ.ФамилияИнициалыФизЛица(СтруктураКомиссии.Ответственный);
		ТабДокумент.Вывести(ОблПодписи);
	КонецЕсли;
КонецПроцедуры


&ИзменениеИКонтроль("ПолучитьДанныеДляПечатнойФормыМ11")
Функция ТК_ПолучитьДанныеДляПечатнойФормыМ11(ПараметрыПечати, МассивДокументов)

	Запрос = Новый Запрос(
	"ВЫБРАТЬ
	|	Товары.Ссылка КАК Ссылка,
	// 4D:ERP для Беларуси, ВладимирР, 23.10.2019 15:02:46 
	// Корр. счет, №23644
	// {
	|	Товары.Номенклатура.ГруппаФинансовогоУчета.СчетУчетаНаСкладе.Код КАК СчетУчета,
	|	Товары.СтатьяРасходов КАК СтатьяРасходов,
	// }
	// 4D
	|	Товары.НомерСтроки КАК НомерСтроки,
	|	Товары.Номенклатура КАК Номенклатура,
	|	Товары.Характеристика КАК Характеристика,
	|	Товары.Упаковка КАК Упаковка,
	|	Товары.КоличествоУпаковок КАК КоличествоУпаковок,
	|	КОНЕЦПЕРИОДА(Товары.Ссылка.Дата, ДЕНЬ) КАК ДатаПолученияЦены,
	|	Товары.Ссылка.ВидЦены КАК ВидЦены,
	|	Товары.Ссылка.ВидЦены.ВалютаЦены КАК ВалютаЦены
	|ПОМЕСТИТЬ Товары
	|ИЗ
	|	Документ.ВнутреннееПотреблениеТоваров.Товары КАК Товары
	|ГДЕ
	|	Товары.Ссылка В(&МассивДокументов)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ПериодыЦенНоменклатуры.Ссылка,
	|	ПериодыЦенНоменклатуры.НомерСтроки,
	|	ЦеныНоменклатуры.Цена,
	|	ЦеныНоменклатуры.Упаковка
	|ПОМЕСТИТЬ Цены
	|ИЗ
	|	(ВЫБРАТЬ
	|		Товары.Ссылка КАК Ссылка,
	|		Товары.НомерСтроки КАК НомерСтроки,
	|		ЦеныНоменклатуры.ВидЦены КАК ВидЦены,
	|		ЦеныНоменклатуры.Номенклатура КАК Номенклатура,
	|		ЦеныНоменклатуры.Характеристика КАК Характеристика,
	|		МАКСИМУМ(ЦеныНоменклатуры.Период) КАК Период
	|	ИЗ
	|		Товары КАК Товары
	|			ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.ЦеныНоменклатуры КАК ЦеныНоменклатуры
	|			ПО Товары.ВидЦены = ЦеныНоменклатуры.ВидЦены
	|				И Товары.Номенклатура = ЦеныНоменклатуры.Номенклатура
	|				И Товары.Характеристика = ЦеныНоменклатуры.Характеристика
	|				И Товары.ДатаПолученияЦены >= ЦеныНоменклатуры.Период
	|				И Товары.ВалютаЦены = ЦеныНоменклатуры.Валюта
	|	
	|	СГРУППИРОВАТЬ ПО
	|		Товары.Ссылка,
	|		Товары.НомерСтроки,
	|		ЦеныНоменклатуры.ВидЦены,
	|		ЦеныНоменклатуры.Номенклатура,
	|		ЦеныНоменклатуры.Характеристика) КАК ПериодыЦенНоменклатуры
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.ЦеныНоменклатуры КАК ЦеныНоменклатуры
	|		ПО ПериодыЦенНоменклатуры.Период = ЦеныНоменклатуры.Период
	|			И ПериодыЦенНоменклатуры.ВидЦены = ЦеныНоменклатуры.ВидЦены
	|			И ПериодыЦенНоменклатуры.Номенклатура = ЦеныНоменклатуры.Номенклатура
	|			И ПериодыЦенНоменклатуры.Характеристика = ЦеныНоменклатуры.Характеристика
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ПериодыКурсовВалютПоДокументам.Ссылка,
	|	КурсыВалют.Валюта,
	|	КурсыВалют.Курс,
	|	КурсыВалют.Кратность
	|ПОМЕСТИТЬ КурсыВалют
	|ИЗ
	|	(ВЫБРАТЬ
	|		Документы.Ссылка КАК Ссылка,
	|		МАКСИМУМ(КурсыВалют.Период) КАК Период,
	|		КурсыВалют.Валюта КАК Валюта
	|	ИЗ
	|		Документ.ВнутреннееПотреблениеТоваров КАК Документы
	|			ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.КурсыВалют КАК КурсыВалют
	|			ПО Документы.ВидЦены.ВалютаЦены = КурсыВалют.Валюта
	|				И Документы.Дата >= КурсыВалют.Период
	|	ГДЕ
	|		Документы.Ссылка В(&МассивДокументов)
	|	
	|	СГРУППИРОВАТЬ ПО
	|		Документы.Ссылка,
	|		КурсыВалют.Валюта) КАК ПериодыКурсовВалютПоДокументам
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.КурсыВалют КАК КурсыВалют
	|		ПО ПериодыКурсовВалютПоДокументам.Период = КурсыВалют.Период
	|			И ПериодыКурсовВалютПоДокументам.Валюта = КурсыВалют.Валюта
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	Документы.Ссылка КАК Ссылка,
	// 4D:ERP для Беларуси, ВладимирР, 23.10.2019 15:02:46 
	// Корр. счет, №23644
	// {
	|	ВЫБОР
	|		КОГДА ТоварыТЧ.СтатьяРасходов ССЫЛКА ПланВидовХарактеристик.СтатьиРасходов
	|			ТОГДА ТоварыТЧ.СтатьяРасходов.СчетУчета.Код
	|		ИНАЧЕ НЕОПРЕДЕЛЕНО
	|	КОНЕЦ КАК КоррСчет,
	// }
	// 4D
	|	Документы.Номер КАК Номер,
	|	Документы.Дата КАК ДатаДокумента,
	|	Документы.Дата КАК ДатаСоставления,
	|	Документы.Организация КАК Организация,
	|	Документы.Организация.Префикс КАК Префикс,
	|	Документы.Подразделение КАК Подразделение,
	|	Документы.ВидЦены КАК УчетныйВидЦены,
	#Вставка
	// 4D:ТКАнрекс, МаксимК,  
	// Комиссии для ПФ, №29014
	// {
	|	Документы.Чд_КомиссияДляПечати КАК Комиссия,
	// } 4D
	#КонецВставки
	|	Документы.ВидЦены.ВалютаЦены КАК УчетныйВидЦеныВалютаЦены
	|ИЗ
	|	Документ.ВнутреннееПотреблениеТоваров КАК Документы
	// 4D:ERP для Беларуси, ВладимирР, 23.10.2019 15:02:46 
	// Корр. счет, №23644
	// {
	|		ЛЕВОЕ СОЕДИНЕНИЕ Товары КАК ТоварыТЧ
	|		ПО (ТоварыТЧ.Ссылка = Документы.Ссылка)
	|			И (ТоварыТЧ.НомерСтроки = 1)
	// }
	// 4D
	|ГДЕ
	|	Документы.Ссылка В(&МассивДокументов)
	|
	|УПОРЯДОЧИТЬ ПО
	|	ДатаДокумента,
	|	Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	Товары.Ссылка КАК Ссылка,
	// 4D:ERP для Беларуси, ВладимирР, 23.10.2019 15:02:46 
	// Корр. счет, №23644
	// {
	|	Товары.СчетУчета КАК Счет,
	// }
	// 4D
	|	Товары.Ссылка.Склад КАК Склад,
	|	ВЫРАЗИТЬ(Товары.Ссылка.Склад КАК Справочник.Склады).ТекущийОтветственный КАК КладовщикОтправитель,
	|	ВЫРАЗИТЬ(Товары.Ссылка.Склад КАК Справочник.Склады).ТекущаяДолжностьОтветственного КАК ДолжностьКладовщикаОтправителя,
	|	Товары.Номенклатура КАК Номенклатура,
	|	Товары.Номенклатура.НаименованиеПолное КАК НоменклатураНаименование,
	|	Товары.Номенклатура.Код КАК НоменклатурныйНомерКод,
	|	Товары.Номенклатура.Артикул КАК НоменклатурныйНомерАртикул,
	|	Товары.Характеристика.НаименованиеПолное КАК Характеристика,
	|	ВЫБОР КОГДА ЕСТЬNULL(&ТекстЗапросаКоэффициентУпаковки1, 1) = 1
	|		ТОГДА НЕОПРЕДЕЛЕНО
	|		ИНАЧЕ Товары.Упаковка.Наименование
	|	КОНЕЦ КАК Упаковка,
	|	&ТекстЗапросаНаименованиеЕдиницыИзмерения КАК ЕдиницаИзмеренияНаименование,
	|	&ТекстЗапросаКодЕдиницыИзмерения КАК ЕдиницаИзмеренияКод,
	#Удаление
	|	Товары.КоличествоУпаковок КАК Количество,
	#КонецУдаления
	#Вставка
	// 4D:ТКАнрекс, МаксимК,  
	// Комиссии для ПФ, №29014
	// {
	|	ЕСТЬNULL(Товары.КоличествоУпаковок, 0) КАК Количество,
	// } 4D
	#КонецВставки
	|	(ВЫРАЗИТЬ(ЕСТЬNULL(Цены.Цена, 0) / ЕСТЬNULL(&ТекстЗапросаКоэффициентУпаковки2, 1) * ЕСТЬNULL(&ТекстЗапросаКоэффициентУпаковки1, 1) * ЕСТЬNULL(КурсыВалют.Курс, 1) / ЕСТЬNULL(КурсыВалют.Кратность, 1) КАК ЧИСЛО(31,2))) * Товары.КоличествоУпаковок КАК Сумма,
	|	ВЫРАЗИТЬ(ЕСТЬNULL(Цены.Цена, 0) / ЕСТЬNULL(&ТекстЗапросаКоэффициентУпаковки2, 1) * ЕСТЬNULL(&ТекстЗапросаКоэффициентУпаковки1, 1) * ЕСТЬNULL(КурсыВалют.Курс, 1) / ЕСТЬNULL(КурсыВалют.Кратность, 1) КАК ЧИСЛО(31,2)) КАК Цена,
	|	Товары.НомерСтроки КАК НомерСтроки
	|ИЗ
	|	Товары КАК Товары
	|		ЛЕВОЕ СОЕДИНЕНИЕ КурсыВалют КАК КурсыВалют
	|		ПО Товары.Ссылка = КурсыВалют.Ссылка
	|		ЛЕВОЕ СОЕДИНЕНИЕ Цены КАК Цены
	|		ПО Товары.Ссылка = Цены.Ссылка
	|			И Товары.НомерСтроки = Цены.НомерСтроки
	|
	|УПОРЯДОЧИТЬ ПО
	|	Ссылка,
	|	НомерСтроки
	|ИТОГИ
	|	МАКСИМУМ(ДолжностьКладовщикаОтправителя), МАКСИМУМ(КладовщикОтправитель)
	|ПО
	|	Ссылка,
	|	Склад");

	Запрос.Текст = СтрЗаменить(Запрос.Текст,
	"&ТекстЗапросаКоэффициентУпаковки1",
	Справочники.УпаковкиЕдиницыИзмерения.ТекстЗапросаКоэффициентаУпаковки(
	"Товары.Упаковка",
	"Товары.Номенклатура"));

	Запрос.Текст = СтрЗаменить(Запрос.Текст,
	"&ТекстЗапросаКоэффициентУпаковки2",
	Справочники.УпаковкиЕдиницыИзмерения.ТекстЗапросаКоэффициентаУпаковки(
	"Цены.Упаковка",
	"Товары.Номенклатура"));

	Запрос.Текст = СтрЗаменить(Запрос.Текст,
	"&ТекстЗапросаНаименованиеЕдиницыИзмерения",
	Справочники.УпаковкиЕдиницыИзмерения.ТекстЗапросаЗначениеРеквизитаЕдиницыИзмерения(
	"Наименование",
	"Товары.Упаковка",
	"Товары.Номенклатура"));

	Запрос.Текст = СтрЗаменить(Запрос.Текст,
	"&ТекстЗапросаКодЕдиницыИзмерения",
	Справочники.УпаковкиЕдиницыИзмерения.ТекстЗапросаЗначениеРеквизитаЕдиницыИзмерения(
	"Код",
	"Товары.Упаковка",
	"Товары.Номенклатура"));

	Запрос.УстановитьПараметр("МассивДокументов", МассивДокументов);

	РезультатПакетаЗапросов = Запрос.ВыполнитьПакет();
	// Пакет запросов:
	// 		[0] - Временная таблица по табличной части документа
	// 		[1] - Временная таблица по ценам номенклатуры табличной части
	// 		[2] - Временная таблица по курсам валют документов
	// 		[3] - Выборка по шапкам документов
	// 		[4] - Выборка по табличным частям документов.

	Возврат Новый Структура(
	"РезультатПоШапке, РезультатПоТабличнойЧасти",
	РезультатПакетаЗапросов[3],
	РезультатПакетаЗапросов[4]);

КонецФункции
// } 4D