﻿#Область ПроцедурыРаботыССериями

// Возвращает значение константы
//
// Параметры:
//  ТипИзмеряемойВеличиныСтрока	 - Строка - тип измеряемой величины строкой
// 
// Возвращаемое значение:
//   СправочникСсылка.УпаковкиЕдиницыИзмерения 
//
&ИзменениеИКонтроль("ЕдиницаИзмеренияПоУмолчанию")
Функция ТК_ЕдиницаИзмеренияПоУмолчанию(ТипИзмеряемойВеличиныСтрока)

	Если ТипИзмеряемойВеличиныСтрока = "Вес" Тогда
		Значение = Константы.ЕдиницаИзмеренияВеса.Получить();
#Вставка
		//4D:"Анрекс", ВикторК, 30.12.202020
		//Задача № 27697 - Перенос доработок с 2.2 в 2.4 с расширением
		// <Создание нового Макета "Спецификация">, № 23117
		// <описание>, №23117
		// {
	ИначеЕсли ТипИзмеряемойВеличиныСтрока = "Чд_ВесБрутто" Тогда
		Значение = Константы.ЕдиницаИзмеренияВеса.Получить();
		// }
		// 4D

#КонецВставки
	ИначеЕсли ТипИзмеряемойВеличиныСтрока = "Объем" Тогда
		Значение = Константы.ЕдиницаИзмеренияОбъема.Получить();
	ИначеЕсли ТипИзмеряемойВеличиныСтрока = "Площадь" Тогда
		Значение = Константы.ЕдиницаИзмеренияПлощади.Получить();
	ИначеЕсли ТипИзмеряемойВеличиныСтрока = "Длина" Тогда 
		Значение = Константы.ЕдиницаИзмеренияДлины.Получить();
	ИначеЕсли ТипИзмеряемойВеличиныСтрока = "Штука" Тогда 
		Значение = Константы.ЕдиницаИзмеренияКоличестваШтук.Получить();
	ИначеЕсли ТипИзмеряемойВеличиныСтрока = "Розлив" Тогда 
		Значение = Константы.ЕдиницаИзмеренияРазливнойПродукции.Получить();
	Иначе
		Значение = Справочники.УпаковкиЕдиницыИзмерения.ПустаяСсылка();
	КонецЕсли;

	Возврат Значение;
КонецФункции

#КонецОбласти