﻿
//&ИзменениеИКонтроль("СформироватьXML")
//Функция ТК_СформироватьXML(СсылкаНаОбъект, СтруктураЭД)

//	Попытка

//		МенеджерОбъекта = ОбщегоНазначения.МенеджерОбъектаПоСсылке(СсылкаНаОбъект);

//		Запрос = Новый Запрос;
//		Запрос.Текст = МенеджерОбъекта.ТекстЗапросаДляЭСЧФ();
//		Запрос.УстановитьПараметр("Ссылка", СсылкаНаОбъект);
//		ТаблицаСчетовФактур		= Запрос.ВыполнитьПакет();

//		Если ТаблицаСчетовФактур = Неопределено ИЛИ ТаблицаСчетовФактур.Количество() = 0 Тогда
//			Возврат Неопределено;
//		КонецЕсли;

//		//////////////////////////////////////////////////////
//		//Шапка
//		//////////////////////////////////////////////////////

//		ДанныеШапки              = ТаблицаСчетовФактур[0].Выбрать();
//		ДанныеШапки.Следующий();

//		ДанныеТабличнойЧасти     = ТаблицаСчетовФактур[1].Выбрать();

//		//ПроверитьСчетФактураВыданный(ДанныеШапки, ДанныеТабличнойЧасти);

//		Если  ДанныеШапки.ТипСчетаФактуры   = Справочники.ТипыЭСЧФ.Исходный Тогда
//			ПространствоИменСхемы = "http://www.w3schools.com";
//		ИначеЕсли ДанныеШапки.ТипСчетаФактуры   = Справочники.ТипыЭСЧФ.Дополнительный Тогда
//			ПространствоИменСхемы = "http://www.w3schoolsa.com";
//		ИначеЕсли ДанныеШапки.ТипСчетаФактуры   = Справочники.ТипыЭСЧФ.Исправленный Тогда	
//			ПространствоИменСхемы = "http://www.w3schoolsf.com";
//		Иначе
//			ПространствоИменСхемы = "http://www.w3schoolsn.com";
//		КонецЕсли;

//		СведенияОПоставщике 			= ФормированиеПечатныхФорм.СведенияОЮрФизЛице(ДанныеШапки.Поставщик,  ДанныеШапки.Дата);
//		СведенияОПолучателе 			= ФормированиеПечатныхФорм.СведенияОЮрФизЛице(ДанныеШапки.Получатель, ДанныеШапки.Дата);

//		НомерЭСЧФ = СокрЛП(ДанныеШапки.ОрганизацияУНП) + "-" + Формат(Год(ДанныеШапки.Дата), "ЧГ=0") + "-" + 
//		Формат(Число(Прав(ПрефиксацияОбъектовКлиентСервер.НомерНаПечать(ДанныеШапки.Номер, Истина, Истина), 10)), "ЧЦ=10; ЧВН=; ЧГ=0"); 

//		ИмяФайла = ОбменСКонтрагентамиСлужебный.ТекущееИмяВременногоФайла("xml");

//		ЗаписьXML  = Новый ЗаписьXML;
//		ЗаписьXML.ОткрытьФайл(ИмяФайла);
//		ЗаписьXML.ЗаписатьОбъявлениеXML();
//		ЗаписьXML.ЗаписатьНачалоЭлемента("issuance");
//		ЗаписьXML.ЗаписатьАтрибут("xmlns", "http://www.w3schools.com");
//		ЗаписьXML.ЗаписатьАтрибут("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
//		ЗаписьXML.ЗаписатьАтрибут("xsi:schemaLocation", "http://www.w3schools.com");
//		ЗаписьXML.ЗаписатьАтрибут("sender", СокрЛП(ДанныеШапки.ОрганизацияУНП));

//		system = ОбменСКонтрагентамиВнутренний.ПолучитьОбъектТипаCML("system", ПространствоИменСхемы);
//		system.modelVersion = "1.0.0";
//		ФабрикаXDTO.ЗаписатьXML(ЗаписьXML,system, "system", , , НазначениеТипаXML.Неявное);

//		general = ОбменСКонтрагентамиВнутренний.ПолучитьОбъектТипаCML("general", ПространствоИменСхемы);

//		general.number           = НомерЭСЧФ;
//		general.documentType     = СокрЛП(ДанныеШапки.ТипСчетаФактурыКод);
//		general.dateTransaction  = Формат(ДанныеШапки.ДатаСовершенияОперации,"ДФ=yyyy-MM-dd");

//		Если ДанныеШапки.ТипСчетаФактуры = Справочники.ТипыЭСЧФ.Исправленный ИЛИ 
//			ДанныеШапки.ТипСчетаФактуры = Справочники.ТипыЭСЧФ.Дополнительный Тогда
//			general.invoice      = СокрЛП(ДанныеШапки.НомерИсходногоДокумента);
//		КонецЕсли;

//		Если   ДанныеШапки.ТипСчетаФактуры = Справочники.ТипыЭСЧФ.Исправленный  Тогда
//			general.dateCancelled  = Формат(ДанныеШапки.ДатаАннулирования,"ДФ=yyyy-MM-dd");
//		КонецЕсли;

//		Если ДанныеШапки.ТипСчетаФактуры = Справочники.ТипыЭСЧФ.Дополнительный Тогда
//			general.sendToRecipient = "true";
//		КонецЕсли;

//		ФабрикаXDTO.ЗаписатьXML(ЗаписьXML,general, "general", , , НазначениеТипаXML.Неявное);

//		Если ДанныеШапки.ТипСчетаФактуры = Справочники.ТипыЭСЧФ.Исходный ИЛИ 
//			ДанныеШапки.ТипСчетаФактуры = Справочники.ТипыЭСЧФ.Исправленный ИЛИ 
//			ДанныеШапки.ТипСчетаФактуры = Справочники.ТипыЭСЧФ.ДополнительныйБезСсылки Тогда 	

//			provider = ОбменСКонтрагентамиВнутренний.ПолучитьОбъектТипаCML("provider", ПространствоИменСхемы);
//			provider.providerStatus          = СокрЛП(ДанныеШапки.СтатусПоставщикаКод); 
//			provider.dependentPerson         = ДанныеШапки.ВзаимозависимоеЛицоПоставщика; 
//			provider.residentsOfOffshore     = ДанныеШапки.СделкаСРезидентомОффшорнойЗоныПоставщика; 
//			provider.specialDealGoods        = ДанныеШапки.ВнешнеторговаяСделкаСоСтратегическимиТоварамиПоставщика; 
//			provider.bigCompany              = ДанныеШапки.ОрганизацияВключеннаяВПереченьКрупныхПлательщиковПоставшика; 
//			provider.countryCode             = СокрЛП(ДанныеШапки.ПоставщикСтранаРегистрацииКод);
//			provider.unp           		 	 = СокрЛП(ДанныеШапки.ПоставщикУНП);
//			Если НЕ ПустаяСтрока(ДанныеШапки.ПоставщикКодФилиала) Тогда
//				provider.branchCode    		 = СокрЛП(ДанныеШапки.ПоставщикКодФилиала);
//			КонецЕсли;
//			provider.name                    = СокрЛП(ДанныеШапки.ПоставщикНаименование); 
//			provider.address       			 = СокрЛП(СведенияОПоставщике.ЮридическийАдрес);

//			Если ЗначениеЗаполнено(ДанныеШапки.РегистрационныйНомерВыпускаТоваровПоставщика) Тогда

//				provider.declaration         = СокрЛП(ДанныеШапки.РегистрационныйНомерВыпускаТоваровПоставщика); 
//				Если ЗначениеЗаполнено(ДанныеШапки.ДатаВыпускаТоваров) Тогда
//					provider.dateRelease     = Формат(ДанныеШапки.ДатаВыпускаТоваровПоставщика,"ДФ=yyyy-MM-dd"); 
//				КонецЕсли;

//				Если ЗначениеЗаполнено(ДанныеШапки.ДатаРазрешенияНаУбытие) Тогда
//					provider.dateActualExport = Формат(ДанныеШапки.ДатаРазрешенияНаУбытиеПоставщика,"ДФ=yyyy-MM-dd");  
//				КонецЕсли;

//			ИначеЕсли ЗначениеЗаполнено(ДанныеШапки.НомерЗаявленияОВвозеТоваровПоставщика) ИЛИ ЗначениеЗаполнено(ДанныеШапки.ДатаЗаявленияОВвозеТоваровПоставщика) Тогда

//				taxes = фабрикаXDTO.Создать(ФабрикаXDTO.Тип(ПространствоИменСхемы, "taxesType"));
//				taxes.number    = СокрЛП(ДанныеШапки.НомерЗаявленияОВвозеТоваровПоставщика);
//				Если ЗначениеЗаполнено(ДанныеШапки.ДатаЗаявленияОВвозеТоваровПоставщика) тогда
//					taxes.date  = Формат(ДанныеШапки.ДатаЗаявленияОВвозеТоваровПоставщика,"ДФ=yyyy-MM-dd");
//				КонецЕсли;
//				provider.taxes = taxes;

//			КонецЕсли;

//			Если ЗначениеЗаполнено(ДанныеШапки.НомерСчетФактурыКомитента) ИЛИ ЗначениеЗаполнено(ДанныеШапки.ДатаСчетФактурыКомитента) Тогда

//				principal = фабрикаXDTO.Создать(ФабрикаXDTO.Тип(ПространствоИменСхемы, "forInvoiceType"));
//				principal.number   = СокрЛП(ДанныеШапки.НомерСчетФактурыКомитента);
//				principal.date     = Формат(ДанныеШапки.ДатаСчетФактурыКомитента, "ДФ=yyyy-MM-dd");
//				provider.principal = principal;

//			КонецЕсли;

//			Если ЗначениеЗаполнено(ДанныеШапки.НомерСчетФактурыПродавца) ИЛИ ЗначениеЗаполнено(ДанныеШапки.ДатаСчетФактурыПродавца) Тогда

//				vendor = фабрикаXDTO.Создать(ФабрикаXDTO.Тип(ПространствоИменСхемы, "forInvoiceType"));
//				vendor.number   = СокрЛП(ДанныеШапки.НомерСчетФактурыПродавца);
//				vendor.date     = Формат(ДанныеШапки.ДатаСчетФактурыПродавца, "ДФ=yyyy-MM-dd");
//				provider.vendor = vendor;

//			КонецЕсли; 

//			ФабрикаXDTO.ЗаписатьXML(ЗаписьXML, provider, "provider", , , НазначениеТипаXML.Неявное);

//			recipient = ОбменСКонтрагентамиВнутренний.ПолучитьОбъектТипаCML("recipient", ПространствоИменСхемы);

//			recipient.recipientStatus        = СокрЛП(ДанныеШапки.СтатусПолучателяКод);
//			recipient.dependentPerson        = ДанныеШапки.ВзаимозависимоеЛицоПолучателя; 
//			recipient.residentsOfOffshore    = ДанныеШапки.СделкаСРезидентомОффшорнойЗоныПолучателя; 
//			recipient.specialDealGoods       = ДанныеШапки.ВнешнеторговаяСделкаСоСтратегическимиТоварамиПолучателя; 
//			recipient.bigCompany             = ДанныеШапки.ОрганизацияВключеннаяВПереченьКрупныхПлательщиковПолучателя; 
//			recipient.countryCode            = СокрЛП(ДанныеШапки.ПолучательСтранаРегистрацииКод);
//			recipient.unp                	 = СокрЛП(ДанныеШапки.ПолучательУНП);
//			Если НЕ ПустаяСтрока(ДанныеШапки.ПолучательКодФилиала) Тогда
//				recipient.branchCode  		 = СокрЛП(ДанныеШапки.ПолучательКодФилиала);
//			КонецЕсли;
//			recipient.name                   = СокрЛП(ДанныеШапки.ПолучательНаименование);
//			recipient.address                = СокрЛП(СведенияОПолучателе.ЮридическийАдрес);

//			Если ЗначениеЗаполнено(ДанныеШапки.РегистрационныйНомерВыпускаТоваровПолучателя) Тогда

//				recipient.declaration        =   СокрЛП(ДанныеШапки.РегистрационныйНомерВыпускаТоваровПолучателя);

//			ИначеЕсли  Значениезаполнено(ДанныеШапки.ДатаВвозаТоваровПолучателя) Тогда

//				recipient.dateImport         =   Формат(ДанныеШапки.ДатаВвозаТоваровПолучателя,"ДФ=yyyy-MM-dd");

//				Если  ЗначениеЗаполнено(ДанныеШапки.НомерЗаявленияОВвозеТоваровПолучателя) 
//					ИЛИ ЗначениеЗаполнено(ДанныеШапки.ДатаЗаявленияОВвозеТоваровПолучателя) Тогда

//					taxes = фабрикаXDTO.Создать(ФабрикаXDTO.Тип(ПространствоИменСхемы, "taxesType"));
//					taxes.number   = ДанныеШапки.НомерЗаявленияОВвозеТоваровПолучателя;;
//					taxes.date     = Формат(ДанныеШапки.ДатаЗаявленияОВвозеТоваровПолучателя,"ДФ=yyyy-MM-dd");
//					recipient.taxes = taxes;

//				КонецЕсли;

//			КонецЕсли;

//			ФабрикаXDTO.ЗаписатьXML(ЗаписьXML, recipient, "recipient", , , НазначениеТипаXML.Неявное);		

//			senderReceiver = ОбменСКонтрагентамиВнутренний.ПолучитьОбъектТипаCML("senderReceiver", ПространствоИменСхемы);

//			Если ДанныеШапки.ВидДокумента = Справочники.ВидыДокументовЭСЧФ.ТТН_1 ИЛИ 
//				ДанныеШапки.ВидДокумента = Справочники.ВидыДокументовЭСЧФ.ЭлектроннаяТТН_1 ИЛИ 
//				ДанныеШапки.СтатусПоставщика = Справочники.СтатусыПоставщикаЭСЧФ.ИностраннаяОрганизация Тогда

//				consignorList  = ОбменСКонтрагентамиВнутренний.ПолучитьОбъектТипаCML("consignorList", ПространствоИменСхемы);
//				consigneesList = ОбменСКонтрагентамиВнутренний.ПолучитьОбъектТипаCML("consigneeList", ПространствоИменСхемы);

//				consignor      = ОбменСКонтрагентамиВнутренний.ПолучитьОбъектТипаCML("consignor", ПространствоИменСхемы);

//				consignor.countryCode   =  СокрЛП(ДанныеШапки.ГрузоотправительСтранаРегистрацииКод);
//				Если НЕ ПустаяСтрока(ДанныеШапки.ГрузоотправительУНП) Тогда
//					consignor.unp       =  СокрЛП(ДанныеШапки.ГрузоотправительУНП);
//				КонецЕсли;
//				consignor.name          =  СокрЛП(ДанныеШапки.ГрузоотправительНаименование);
//				consignor.address       =  СокрЛП(ДанныеШапки.АдресГрузоотправителя);

//				consignorList.consignor.Добавить(consignor);
//				senderReceiver.consignors  =    consignorList;

//				consignee      = ОбменСКонтрагентамиВнутренний.ПолучитьОбъектТипаCML("consignee", ПространствоИменСхемы);

//				consignee.countryCode   =  СокрЛП(ДанныеШапки.ГрузополучательСтранаРегистрацииКод);
//				#Вставка
//				// 4D:"Анрэкс", МаксимК, 30.06.2022
//				//  Доработать загрузку-выгрузку ЭСЧФ, №ANKT-179
//				// {
//				Если ДанныеШапки.Контрагент.ЭЭ_СубъектХозяйствования Тогда
//					consignee.unp           =  "000000000";
//					consignee.name          =  СведенияОПокупателе.ОфициальноеНаименование;
//				КонецЕсли;
//				//
//				// } 4D
//				#КонецВставки
//				Если НЕ ПустаяСтрока(ДанныеШапки.ГрузополучательУНП) Тогда
//					consignee.unp       =  СокрЛП(ДанныеШапки.ГрузополучательУНП);
//				КонецЕсли;
//				consignee.name          =  СокрЛП(ДанныеШапки.ГрузополучательНаименование);
//				consignee.address       =  СокрЛП(ДанныеШапки.АдресГрузополучателя);

//				consigneesList.consignee.Добавить(consignee);
//				senderReceiver.consignees  =    consigneesList;

//			КонецЕсли;

//			ФабрикаXDTO.ЗаписатьXML(ЗаписьXML, senderReceiver, "senderReceiver", , , НазначениеТипаXML.Неявное);

//		ИначеЕсли ДанныеШапки.ТипСчетаФактуры = Справочники.ТипыЭСЧФ.Дополнительный Тогда 

//			recipient = ОбменСКонтрагентамиВнутренний.ПолучитьОбъектТипаCML("recipient", ПространствоИменСхемы);

//			Если  ЗначениеЗаполнено(ДанныеШапки.НомерЗаявленияОВвозеТоваровПолучателя) 
//				ИЛИ ЗначениеЗаполнено(ДанныеШапки.ДатаЗаявленияОВвозеТоваровПолучателя) Тогда

//				taxes = фабрикаXDTO.Создать(ФабрикаXDTO.Тип(ПространствоИменСхемы, "taxesType"));
//				taxes.number   = ДанныеШапки.НомерЗаявленияОВвозеТоваровПолучателя;;
//				taxes.date     = Формат(ДанныеШапки.ДатаЗаявленияОВвозеТоваровПолучателя,"ДФ=yyyy-MM-dd");
//				recipient.taxes = taxes;

//			КонецЕсли;

//			ФабрикаXDTO.ЗаписатьXML(ЗаписьXML, recipient, "recipient", , , НазначениеТипаXML.Неявное);		

//		КонецЕсли;

//		deliveryCondition = ОбменСКонтрагентамиВнутренний.ПолучитьОбъектТипаCML("deliveryCondition", ПространствоИменСхемы);
//		Если ЗначениеЗаполнено(ДанныеШапки.ДополнительныеСведения) Тогда
//			deliveryCondition.description   = ДанныеШапки.ДополнительныеСведенияКод;
//		КонецЕсли;	
//		deliveryCondition.belongToString   = "1.1";

//		contract = ОбменСКонтрагентамиВнутренний.ПолучитьОбъектТипаCML("contract", ПространствоИменСхемы);

//		Если  ЗначениеЗаполнено(ДанныеШапки.НомерДоговора) Тогда

//			contract.number = СокрЛП(ДанныеШапки.НомерДоговора); 
//			contract.date   = Формат(ДанныеШапки.ДатаДоговора, "ДФ=yyyy-MM-dd");

//		КонецЕсли;

//		Если ЗначениеЗаполнено(ДанныеШапки.НомерИсходящегоДокумента) Тогда

//			documentList   = ОбменСКонтрагентамиВнутренний.ПолучитьОбъектТипаCML("documentList", ПространствоИменСхемы); 
//			document       = ОбменСКонтрагентамиВнутренний.ПолучитьОбъектТипаCML("document",     ПространствоИменСхемы); 

//			docType        = ОбменСКонтрагентамиВнутренний.ПолучитьОбъектТипаCML("docType", ПространствоИменСхемы);
//			docType.code   = ДанныеШапки.ВидДокумента.Код;
//			docType.value  = Строка(ДанныеШапки.ВидДокумента.Наименование);

//			document.date 	      = Формат(ДанныеШапки.ДатаИсходящегоДокумента,"ДФ=yyyy-MM-dd");
//			document.blankCode    = Строка(СокрЛП(ДанныеШапки.КодБланкаИсходящегоДокумента)); 
//			document.seria        = Строка(СокрЛП(ДанныеШапки.СерияИсходящегоДокумента));
//			document.number       = Строка(СокрЛП(ДанныеШапки.НомерИсходящегоДокумента));

//			Если СсылкаНаОбъект.ВозвратТовара Тогда
//				document.refund = Истина;
//			КонецЕсли;

//			document.docType      = docType;
//			documentList.document.Добавить(document);
//			contract.documents =  documentList;

//		КонецЕсли;

//		Если  ЗначениеЗаполнено(ДанныеШапки.НомерДоговора) ИЛИ ЗначениеЗаполнено(ДанныеШапки.НомерИсходящегоДокумента) Тогда
//			deliveryCondition.contract = contract;
//		КонецЕсли;

//		ФабрикаXDTO.ЗаписатьXML(ЗаписьXML, deliveryCondition, "deliveryCondition",,, НазначениеТипаXML.Неявное);

//		rosterList   = ОбменСКонтрагентамиВнутренний.ПолучитьОбъектТипаCML("rosterList", ПространствоИменСхемы);

//		//////////////////////////////////////////////////////
//		//Табличная часть
//		//////////////////////////////////////////////////////

//		totalCost    = 0;
//		totalExcise  = 0;
//		totalCostVat = 0;
//		totalVat     = 0;

//		Пока ДанныеТабличнойЧасти.Следующий() Цикл

//			Если ДанныеТабличнойЧасти.ИспользоватьРасчетнуюСтавкуНДС Тогда

//				ТипСтавки = "CALCULATED";
//				ПроцентНДС = ДанныеТабличнойЧасти.РасчетнаяСтавкаНДС;

//			Иначе

//				ПроцентНДС = УчетНДСУПКлиентСервер.ПолучитьСтавкуНДС(ДанныеТабличнойЧасти.СтавкаНДС);
//				Если ДанныеТабличнойЧасти.СтавкаНДС = Перечисления.СтавкиНДС.НДС0 Тогда
//					ТипСтавки = "ZERO";
//				ИначеЕсли ДанныеТабличнойЧасти.СтавкаНДС = Перечисления.СтавкиНДС.БезНДС Тогда
//					ТипСтавки = "NO_VAT";
//				Иначе
//					ТипСтавки = "DECIMAL";
//				КонецЕсли;

//			КонецЕсли;

//			rosterItem             = ОбменСКонтрагентамиВнутренний.ПолучитьОбъектТипаCML("rosterItem",
//			ПространствоИменСхемы);
//			rosterItem.number      = ДанныеТабличнойЧасти.НомерСтроки;

//			КодТНВЭД = ?(ЗначениеЗаполнено(ДанныеТабличнойЧасти.КодТНВЭДЕАС), СокрЛП(ДанныеТабличнойЧасти.КодТНВЭДЕАС), "");
//			Если ЗначениеЗАполнено(КодТНВЭД) Тогда
//				rosterItem.code        = КодТНВЭД;
//			КонецЕсли;

//			КодОКЭД  = ?(ЗначениеЗаполнено(ДанныеТабличнойЧасти.КодОКЭД), Число(ДанныеТабличнойЧасти.КодОКЭД), "");
//			Если ЗначениеЗаполнено(КодОКЭД) Тогда
//				rosterItem.code_oced   = КодОКЭД;
//			КонецЕсли;

//			Если ТипЗнч(ДанныеТабличнойЧасти.Номенклатура) = Тип("Строка") Тогда
//				rosterItem.name        = ДанныеТабличнойЧасти.Номенклатура;
//			Иначе
//				rosterItem.name        = ДанныеТабличнойЧасти.Наименование;
//			КонецЕсли;

//			rosterItem.count       = ДанныеТабличнойЧасти.Количество;
//			rosterItem.price       = ДанныеТабличнойЧасти.Цена;
//			rosterItem.cost        = ДанныеТабличнойЧасти.Стоимость;
//			rosterItem.summaExcise = ДанныеТабличнойЧасти.СтоимостьАкциза;
//			rosterItem.costVat     = ДанныеТабличнойЧасти.Всего;
//			Если ЗначениеЗаполнено(ДанныеТабличнойЧасти.КодЕдИзм) Тогда
//				rosterItem.units   = Число(ДанныеТабличнойЧасти.КодЕдИзм);
//			КонецЕсли;

//			vat                    = ОбменСКонтрагентамиВнутренний.ПолучитьОбъектТипаCML("vat", ПространствоИменСхемы);
//			vat.rate               = ПроцентНДС;
//			vat.ratetype           = Строка(ТипСтавки);
//			vat.summaVat           = ДанныеТабличнойЧасти.СуммаНДС;
//			rosterItem.vat         = vat;

//			totalCost    = totalCost + rosterItem.cost;
//			totalExcise  = totalExcise + rosterItem.summaExcise;
//			totalCostVat = totalCostVat + rosterItem.costVat;
//			totalVat     = totalVat + vat.summaVat;

//			Если ЗначениеЗаполнено(ДанныеТабличнойЧасти.ДополнительныеДанные) Тогда

//				descriptions = ОбменСКонтрагентамиВнутренний.ПолучитьОбъектТипаCML("descriptionList", ПространствоИменСхемы);
//				descriptions.description.Добавить(ДанныеТабличнойЧасти.ДополнительныеДанныеКод);
//				ОбменСКонтрагентамиВнутренний.ЗаполнитьСвойствоXDTO(rosterItem, "descriptions", descriptions);

//			КонецЕсли;

//			rosterList.rosterItem.Добавить(rosterItem);

//		КонецЦикла;

//		rosterList.totalCost    = totalCost;
//		rosterList.totalExcise  = totalExcise;
//		rosterList.totalCostVat = totalCostVat;
//		rosterList.totalVat     = totalVat;

//		ФабрикаXDTO.ЗаписатьXML(ЗаписьXML, rosterList, "roster", , , НазначениеТипаXML.Неявное);

//		ЗаписьXML.ЗаписатьКонецЭлемента();

//		ЗаписьXML.Закрыть();

//		СтруктураЭД.Вставить("НомерЭД", НомерЭСЧФ);
//		СтруктураЭД.Вставить("ИмяБезРасширения", НомерЭСЧФ);
//		СтруктураЭД.Вставить("ИмяФайла", ИмяФайла);

//		Возврат УдалитьПространствоИмен(СтруктураЭД.ИмяФайла, ПространствоИменСхемы);

//	Исключение

//		ШаблонСообщения = НСтр("ru='Ошибка заполнения документа: %1'");
//		ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ШаблонСообщения, ОписаниеОшибки());
//		ОбщегоНазначения.СообщитьПользователю(ТекстСообщения, СсылкаНаОбъект);

//		Возврат Ложь;

//	КонецПопытки;

//КонецФункции
