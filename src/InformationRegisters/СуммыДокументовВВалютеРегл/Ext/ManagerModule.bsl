﻿
&ИзменениеИКонтроль("РассчитатьПоДаннымВзаиморасчетов")
Процедура ТК_РассчитатьПоДаннымВзаиморасчетов(ТипРасчетов, МассивДокументов)

	Запрос = Новый Запрос();
	Запрос.Текст = 
	"ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	ДанныеРегистра.Регистратор КАК Регистратор
	|
	|ПОМЕСТИТЬ ТаблицаДокументов
	|ИЗ
	|	РегистрСведений.СуммыДокументовВВалютеРегл КАК ДанныеРегистра
	|
	|ГДЕ
	|	ДанныеРегистра.ТипРасчетов = &ТипРасчетов
	|	И (ДанныеРегистра.Валюта <> &ВалютаРегламентированногоУчета 
	|		ИЛИ ДанныеРегистра.Валюта <> &ВалютаУправленческогоУчета
	|		ИЛИ ДанныеРегистра.Валюта <> ДанныеРегистра.ВалютаВзаиморасчетов)
	|	И ДанныеРегистра.Регистратор В (&МассивДокументов)
	|;
	|
	|///////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	ДанныеРегистра.Регистратор КАК Ссылка,
	|	ДанныеРегистра.Активность КАК Активность,
	|	ДанныеРегистра.Период КАК Период,
	|	ДанныеРегистра.ИдентификаторСтроки КАК ИдентификаторСтроки,
	|
	|	ДанныеРегистра.СуммаБезНДС          КАК СуммаБезНДС,
	|	ДанныеРегистра.СуммаНДС             КАК СуммаНДС,
	|	ДанныеРегистра.СуммаБезНДСРегл      КАК СуммаБезНДСРегл,
	|	ДанныеРегистра.СуммаНДСРегл         КАК СуммаНДСРегл,
	|	ДанныеРегистра.БазаНДСРегл          КАК БазаНДСРегл,
	|	ДанныеРегистра.СуммаБезНДСУпр       КАК СуммаБезНДСУпр,
	|	ДанныеРегистра.СуммаНДСУпр          КАК СуммаНДСУпр,
	|	ДанныеРегистра.БазаНДСУпр           КАК БазаНДСУпр,
	|	ДанныеРегистра.СуммаВзаиморасчетов  КАК СуммаВзаиморасчетов,
	|
	|	ДанныеРегистра.Валюта                    КАК Валюта,
	|	ДанныеРегистра.ВалютаВзаиморасчетов      КАК ВалютаВзаиморасчетов,
	|	ДанныеРегистра.СтавкаНДС                 КАК СтавкаНДС,
	|	ДанныеРегистра.ТипРасчетов               КАК ТипРасчетов,
	|	ДанныеРегистра.АналитикаУчетаПоПартнерам КАК АналитикаУчетаПоПартнерам,
	|
	|	ВЫБОР
	|		КОГДА ДанныеРегистра.ТипРасчетов = &ТипРасчетов
	|			И (ДанныеРегистра.СуммаБезНДС + ДанныеРегистра.СуммаНДС) <> 0
	|		ТОГДА ИСТИНА
	|		ИНАЧЕ ЛОЖЬ
	|	КОНЕЦ КАК ПересчитатьСуммы,
	|
	|	ВЫБОР КОГДА ДанныеРегистра.ТипРасчетов = &ТипРасчетов
	|		ТОГДА ДанныеРегистра.СуммаБезНДС + ДанныеРегистра.СуммаНДС
	|		ИНАЧЕ 0
	|	КОНЕЦ КАК СуммаСНДС,
	|
	|	ВЫБОР КОГДА ДанныеРегистра.ТипРасчетов = &ТипРасчетов
	|		ТОГДА ДанныеРегистра.СуммаВзаиморасчетов
	|		ИНАЧЕ 0
	|	КОНЕЦ КАК СуммаВзаиморасчетовДляПересчета,
	|
	|	ВЫБОР КОГДА ДанныеРегистра.СуммаБезНДС + ДанныеРегистра.СуммаНДС < 0
	|		ТОГДА ИСТИНА
	|		ИНАЧЕ ЛОЖЬ
	|	КОНЕЦ КАК ЭтоСторно
	|
	|ИЗ
	|	РегистрСведений.СуммыДокументовВВалютеРегл КАК ДанныеРегистра
	|	ВНУТРЕННЕЕ СОЕДИНЕНИЕ
	|		ТаблицаДокументов КАК ТаблицаДокументов
	|	ПО
	|		ДанныеРегистра.Регистратор = ТаблицаДокументов.Регистратор
	|	
	|ИТОГИ
	|	МАКСИМУМ(Период),
	|	СУММА(СуммаСНДС),
	|	СУММА(СуммаБезНДСРегл),
	|	СУММА(СуммаНДСРегл),
	|	СУММА(СуммаБезНДСУпр),
	|	СУММА(СуммаНДСУпр),
	|	СУММА(СуммаВзаиморасчетовДляПересчета),
	|	МАКСИМУМ(ВалютаВзаиморасчетов),
	|	МАКСИМУМ(Валюта),
	|	МАКСИМУМ(ТипРасчетов)
	|ПО
	|	ДанныеРегистра.Регистратор КАК Ссылка,
	|	ДанныеРегистра.АналитикаУчетаПоПартнерам КАК АналитикаУчетаПоПартнерам,
	|	ВЫБОР КОГДА ДанныеРегистра.СуммаБезНДС + ДанныеРегистра.СуммаНДС < 0
	|		ТОГДА ИСТИНА
	|		ИНАЧЕ ЛОЖЬ
	|	КОНЕЦ КАК ЭтоСторно
	|;
	|
	|///////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ТаблицаДокументов.Регистратор КАК Регистратор
	|ИЗ
	|	ТаблицаДокументов КАК ТаблицаДокументов
	|";
	Запрос.УстановитьПараметр("МассивДокументов", МассивДокументов);

	Валюты = Новый Структура("ВалютаРегламентированногоУчета, ВалютаУправленческогоУчета, КэшКурсовВалют",
	Константы.ВалютаРегламентированногоУчета.Получить(),
	Константы.ВалютаУправленческогоУчета.Получить(),
	РаботаСКурсамиВалютУТ.ИнициализироватьКэшКурсовВалют());
	Запрос.УстановитьПараметр("ВалютаРегламентированногоУчета", Валюты.ВалютаРегламентированногоУчета);
	Запрос.УстановитьПараметр("ВалютаУправленческогоУчета", Валюты.ВалютаУправленческогоУчета);

	Запрос.УстановитьПараметр("ТипРасчетов", 
	?(ТипРасчетов = "РасчетыСКлиентами",
	Перечисления.ТипыРасчетовСПартнерами.РасчетыСКлиентом,
	Перечисления.ТипыРасчетовСПартнерами.РасчетыСПоставщиком));

	МассивРезультатов = Запрос.ВыполнитьПакет();

	РезультатПоДокументам = МассивРезультатов[1];
	ДокументыКПересчету   = МассивРезультатов[2].Выгрузить().ВыгрузитьКолонку("Регистратор");
	ТаблицаСуммДляРаспределения = СуммыДокументовДляПострочногоРаспределения(ДокументыКПересчету);
	ТаблицаСуммДляРаспределения.Индексы.Добавить("Регистратор, АналитикаУчетаПоПартнерам, ТипРасчетов");

	ТаблицаСуммыДокумента = РегистрыСведений.СуммыДокументовВВалютеРегл.СоздатьНаборЗаписей().ВыгрузитьКолонки();
	ТаблицаСуммыДокумента.Колонки.Добавить("СуммаСНДСРегл", ОбщегоНазначенияУТ.ОписаниеТипаДенежногоПоля());
	ТаблицаСуммыДокумента.Колонки.Добавить("СуммаСНДСУпр", ОбщегоНазначенияУТ.ОписаниеТипаДенежногоПоля());
	ТаблицаСуммыДокумента.Колонки.Добавить("СуммаДляБазыНДС", ОбщегоНазначенияУТ.ОписаниеТипаДенежногоПоля());

	ВыборкаПоДокументу = РезультатПоДокументам.Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);

	Пока ВыборкаПоДокументу.Следующий() Цикл

		КурсВалютыДокумента = 
		РаботаСКурсамиВалютУТ.ПолучитьКурсВалютыИзКэша(
		ВыборкаПоДокументу.Валюта,
		НачалоДня(ВыборкаПоДокументу.Период),
		Валюты.КэшКурсовВалют);

		КурсВалютыВзаиморасчетов = 
		РаботаСКурсамиВалютУТ.ПолучитьКурсВалютыИзКэша(
		ВыборкаПоДокументу.ВалютаВзаиморасчетов,
		НачалоДня(ВыборкаПоДокументу.Период),
		Валюты.КэшКурсовВалют);

		КурсВалютыУпр = 
		РаботаСКурсамиВалютУТ.ПолучитьКурсВалютыИзКэша(
		Валюты.ВалютаУправленческогоУчета,
		НачалоДня(ВыборкаПоДокументу.Период),
		Валюты.КэшКурсовВалют);

		// 4D:ERP для Беларуси, АндрейБ,  20.05.2020 
		// Задача №25489 "В случае использования Заказа клиента с детализацией расчетов "По заказам", в валюте и
		// вариантом оплаты  - "Предоплата до отгрузки", в созданном на основании РТИУ при формировании проводок в регл.учете
		// сумма НДС рассчитывается по курсу на дату создания РТИУ, а не на дату предоплаты"
		// {
		Если (ТипЗнч(ВыборкаПоДокументу.Ссылка) = Тип("ДокументСсылка.РеализацияТоваровУслуг") Или ТипЗнч(ВыборкаПоДокументу.Ссылка) = Тип("ДокументСсылка.АктВыполненныхРабот")
			ИЛИ ТипЗнч(ВыборкаПоДокументу.Ссылка) = Тип("ДокументСсылка.ОтчетДавальцу")) Тогда

			КурсыУсредненные       = ПересчитатьКурсСУчетомПредоплаты(ВыборкаПоДокументу.Ссылка, Ложь);

			КурсВалютыДокументаТек = КурсВалютыДокумента;
			КурсВалютыДокумента    = КурсыУсредненные.КоэффициентПересчетаВВалютуРегл;

			КурсВалютыУпрТек       = КурсВалютыУпр;
			КурсВалютыУпр          = КурсВалютыДокумента / КурсыУсредненные.КоэффициентПересчетаВВалютуУПР;

			КурсВалютыВзаиморасчетовТек = КурсВалютыВзаиморасчетов;
			КурсВалютыВзаиморасчетов    = КурсВалютыДокумента / КурсыУсредненные.КоэффициентПересчетаВВалютуВзаиморасчетов;
		КонецЕсли; 
		// }
		// 4D 

		ВыборкаПоАналитике = ВыборкаПоДокументу.Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);
		Пока ВыборкаПоАналитике.Следующий() Цикл

			СтруктураПоиска = Новый Структура;
			СтруктураПоиска.Вставить("Регистратор",  ВыборкаПоДокументу.Ссылка);
			СтруктураПоиска.Вставить("ТипРасчетов",  ВыборкаПоАналитике.ТипРасчетов);
			Если ЗначениеЗаполнено(ВыборкаПоАналитике.АналитикаУчетаПоПартнерам) Тогда
				СтруктураПоиска.Вставить("АналитикаУчетаПоПартнерам", ВыборкаПоАналитике.АналитикаУчетаПоПартнерам);
			КонецЕсли;
			СуммыПоДокументу = ТаблицаСуммДляРаспределения.НайтиСтроки(СтруктураПоиска);

			ВыборкаПоТипуЗаписи = ВыборкаПоАналитике.Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);
			Пока ВыборкаПоТипуЗаписи.Следующий() Цикл

				СуммаДокумента      = ВыборкаПоТипуЗаписи.СуммаСНДС;
				СуммаВзаиморасчетов = ВыборкаПоТипуЗаписи.СуммаВзаиморасчетовДляПересчета;

				СуммаДокументаРегл  = 0;
				СуммаДокументаУпр   = 0;

				Предоплата         = 0;
				ПредоплатаРегл     = 0; 
				ПредоплатаУпр      = 0;
				Долг               = 0;
				ДолгРегл           = 0;
				ДолгУпр            = 0;

				ПорядокОплаты      = Неопределено;
				НалогообложениеНДС = Неопределено;

				Если СуммыПоДокументу.Количество() = 1 Тогда
					Если ВыборкаПоТипуЗаписи.ЭтоСторно Тогда
						ПредоплатаРегл = ?(СуммыПоДокументу[0].Предоплата <= 0, СуммыПоДокументу[0].ПредоплатаРегл, 0);
						ПредоплатаУпр  = ?(СуммыПоДокументу[0].Предоплата <= 0, СуммыПоДокументу[0].ПредоплатаУпр, 0);
						ДолгРегл       = ?(СуммыПоДокументу[0].Долг <= 0, СуммыПоДокументу[0].ДолгРегл, 0);
						ДолгУпр        = ?(СуммыПоДокументу[0].Долг <= 0, СуммыПоДокументу[0].ДолгУпр, 0);
						Предоплата     = ?(СуммыПоДокументу[0].Предоплата <= 0, СуммыПоДокументу[0].Предоплата, 0);
						Долг            = ?(СуммыПоДокументу[0].Долг <= 0, СуммыПоДокументу[0].Долг, 0);
					Иначе
						ПредоплатаРегл = ?(СуммыПоДокументу[0].Предоплата >= 0, СуммыПоДокументу[0].ПредоплатаРегл, 0);
						ПредоплатаУпр  = ?(СуммыПоДокументу[0].Предоплата >= 0, СуммыПоДокументу[0].ПредоплатаУпр, 0);
						ДолгРегл       = ?(СуммыПоДокументу[0].Долг >= 0, СуммыПоДокументу[0].ДолгРегл, 0);
						ДолгУпр        = ?(СуммыПоДокументу[0].Долг >= 0, СуммыПоДокументу[0].ДолгУпр, 0);
						Предоплата     = ?(СуммыПоДокументу[0].Предоплата >= 0, СуммыПоДокументу[0].Предоплата, 0);
						Долг           = ?(СуммыПоДокументу[0].Долг >= 0, СуммыПоДокументу[0].Долг, 0);
					КонецЕсли;
					ПорядокОплаты  = СуммыПоДокументу[0].ПорядокОплаты;
					НалогообложениеНДС = СуммыПоДокументу[0].НалогообложениеНДС;
				КонецЕсли;

				Если Предоплата + Долг <> СуммаВзаиморасчетов Тогда
					СуммаДокументаРегл = ПредоплатаРегл
					+ (СуммаВзаиморасчетов - Предоплата) * КурсВалютыВзаиморасчетов;
					СуммаДокументаУпр = ПредоплатаУпр 
					+ (СуммаВзаиморасчетов - Предоплата) * КурсВалютыВзаиморасчетов / КурсВалютыУпр;
				Иначе
					СуммаДокументаРегл = ПредоплатаРегл + ДолгРегл;
					СуммаДокументаУпр = ПредоплатаУпр + ДолгУпр;
				КонецЕсли;

				Если ВыборкаПоТипуЗаписи.ЭтоСторно Тогда
					БазаСНДСРеглПоДокументу = ПредоплатаРегл + ДолгРегл;
				Иначе

					// 4D:ERP для Беларуси, АндрейБ,  20.05.2020 
					// Задача №25489 "В случае использования Заказа клиента с детализацией расчетов "По заказам", в валюте и
					// вариантом оплаты  - "Предоплата до отгрузки", в созданном на основании РТИУ при формировании проводок в регл.учете
					// сумма НДС рассчитывается по курсу на дату создания РТИУ, а не на дату предоплаты"
					// {
					Если (ТипЗнч(ВыборкаПоДокументу.Ссылка) = Тип("ДокументСсылка.РеализацияТоваровУслуг") Или ТипЗнч(ВыборкаПоДокументу.Ссылка) = Тип("ДокументСсылка.АктВыполненныхРабот")
						ИЛИ ТипЗнч(ВыборкаПоДокументу.Ссылка) = Тип("ДокументСсылка.ОтчетДавальцу")) Тогда

						БазаСНДСРеглПоДокументу = ПредоплатаРегл + (СуммаВзаиморасчетов - Предоплата) * КурсВалютыВзаиморасчетовТек;
					Иначе
						БазаСНДСРеглПоДокументу = ПредоплатаРегл + (СуммаВзаиморасчетов - Предоплата) * КурсВалютыВзаиморасчетов;
					КонецЕсли;
					// }
					// 4D

				КонецЕсли;

				УчтеноБазыРаспределения = 0;
				УжеРаспределеноРегл = 0;
				УжеРаспределеноУпр = 0;
				УжеРаспределеноСуммаДляБазыНДС = 0;

				Выборка = ВыборкаПоТипуЗаписи.Выбрать();
				Пока Выборка.Следующий() Цикл

					Запись = ТаблицаСуммыДокумента.Добавить();
					ЗаполнитьЗначенияСвойств(Запись, Выборка);

					Если НЕ Выборка.ПересчитатьСуммы Тогда
						Продолжить;
					КонецЕсли;

					СуммаСНДС = Выборка.СуммаБезНДС + Выборка.СуммаНДС;

					Запись.СуммаСНДСРегл = Окр(СуммаДокументаРегл * (УчтеноБазыРаспределения + СуммаСНДС) / СуммаДокумента, 2) - УжеРаспределеноРегл;
					Запись.СуммаСНДСУпр  = Окр(СуммаДокументаУпр * (УчтеноБазыРаспределения + СуммаСНДС) / СуммаДокумента, 2) - УжеРаспределеноУпр;
					Запись.СуммаДляБазыНДС  = Окр(БазаСНДСРеглПоДокументу * (УчтеноБазыРаспределения + СуммаСНДС) / СуммаДокумента, 2) - УжеРаспределеноСуммаДляБазыНДС;

					УчтеноБазыРаспределения = УчтеноБазыРаспределения + СуммаСНДС;
					УжеРаспределеноРегл     = УжеРаспределеноРегл + Запись.СуммаСНДСРегл;
					УжеРаспределеноУпр      = УжеРаспределеноУпр + Запись.СуммаСНДСУпр;
					УжеРаспределеноСуммаДляБазыНДС = УжеРаспределеноСуммаДляБазыНДС + Запись.СуммаДляБазыНДС; 

					Если (ВыборкаПоДокументу.ВалютаВзаиморасчетов = Валюты.ВалютаРегламентированногоУчета
						ИЛИ ВыборкаПоДокументу.Валюта = Валюты.ВалютаРегламентированногоУчета)
						И ((ВыборкаПоТипуЗаписи.СуммаБезНДСРегл + ВыборкаПоТипуЗаписи.СуммаНДСРегл) = СуммаДокументаРегл) Тогда
						ПересчитыватьРегл = Ложь;
					Иначе
						ПересчитыватьРегл = Истина;
					КонецЕсли;

					Если (ВыборкаПоДокументу.ВалютаВзаиморасчетов = Валюты.ВалютаУправленческогоУчета
						ИЛИ ВыборкаПоДокументу.Валюта = Валюты.ВалютаУправленческогоУчета)
						И ((ВыборкаПоТипуЗаписи.СуммаБезНДСУпр + ВыборкаПоТипуЗаписи.СуммаНДСУпр) = СуммаДокументаУпр) Тогда
						ПересчитыватьУпр = Ложь;
					Иначе
						ПересчитыватьУпр = Истина;
					КонецЕсли;

					Если ПересчитыватьРегл Тогда
						Если ЗначениеЗаполнено(Запись.СтавкаНДС) Тогда
							// Рассчитаем НДС и суммы без НДС в валюте регл. и упр. учета
							Если ПорядокОплаты = Перечисления.ПорядокОплатыПоСоглашениям.РасчетыВВалютеОплатаВВалюте Тогда

								// База НДС рассчитывается по курсу на дату документа
								// 4D:ERP для Беларуси, АндрейБ,  20.05.2020 
								// Задача №25489 "В случае использования Заказа клиента с детализацией расчетов "По заказам", в валюте и
								// вариантом оплаты  - "Предоплата до отгрузки", в созданном на основании РТИУ при формировании проводок в регл.учете
								// сумма НДС рассчитывается по курсу на дату создания РТИУ, а не на дату предоплаты"
								// {
								Если ТипЗнч(ВыборкаПоДокументу.Ссылка) = Тип("ДокументСсылка.РеализацияТоваровУслуг") Или ТипЗнч(ВыборкаПоДокументу.Ссылка) = Тип("ДокументСсылка.АктВыполненныхРабот")
									ИЛИ ТипЗнч(ВыборкаПоДокументу.Ссылка) = Тип("ДокументСсылка.ОтчетДавальцу") Тогда

									СтавкаНДС = ДополнительныйСервер_Локализация.ПолучитьСтавкуНДС(Запись.СтавкаНДС);
									Запись.БазаНДСРегл = Запись.СуммаДляБазыНДС * 100 / (100 + СтавкаНДС);
								Иначе
									Запись.БазаНДСРегл = Запись.СуммаБезНДС * КурсВалютыДокумента;
								КонецЕсли;
								// }
								// 4D

								Если Запись.СуммаНДС = 0 Тогда 
									Запись.СуммаНДСРегл = 0;
								Иначе
									Запись.СуммаНДСРегл = ЦенообразованиеКлиентСервер.РассчитатьСуммуНДС(Запись.БазаНДСРегл, Выборка.СтавкаНДС, Ложь);
								КонецЕсли;
							Иначе
								// Расчет базы НДС по данным взаиморасчетов (с учетом зачтенных предоплат)
								Если Запись.СуммаНДС = 0 Тогда 
									Запись.СуммаНДСРегл = 0;
								Иначе
									#Вставка
									//++EE:IVI 23.03.2022 ANKT-121
									Если ТипЗнч(ВыборкаПоДокументу.Ссылка) = Тип("ДокументСсылка.РеализацияТоваровУслуг") Тогда
										Запись.СуммаНДСРегл = Запись.СуммаНДС;
									Иначе
										Запись.СуммаНДСРегл = ЦенообразованиеКлиентСервер.РассчитатьСуммуНДС(Запись.СуммаДляБазыНДС, Запись.СтавкаНДС);
									КонецЕсли;
									//--EE:IVI 23.03.2022 ANKT-121
									#КонецВставки
									#Удаление
									Запись.СуммаНДСРегл = ЦенообразованиеКлиентСервер.РассчитатьСуммуНДС(Запись.СуммаДляБазыНДС, Запись.СтавкаНДС);
									#КонецУдаления
								КонецЕсли;
								Запись.БазаНДСРегл = Запись.СуммаДляБазыНДС - Запись.СуммаНДСРегл;
							КонецЕсли;
						КонецЕсли;
						Запись.СуммаБезНДСРегл = Запись.СуммаСНДСРегл - Запись.СуммаНДСРегл;
					КонецЕсли;

					Если ПересчитыватьУпр ИЛИ ПересчитыватьРегл Тогда

						// 4D:ERP для Беларуси, АндрейБ,  20.05.2020 
						// Задача №25489 "В случае использования Заказа клиента с детализацией расчетов "По заказам", в валюте и
						// вариантом оплаты  - "Предоплата до отгрузки", в созданном на основании РТИУ при формировании проводок в регл.учете
						// сумма НДС рассчитывается по курсу на дату создания РТИУ, а не на дату предоплаты"
						// {
						Если ТипЗнч(ВыборкаПоДокументу.Ссылка) = Тип("ДокументСсылка.РеализацияТоваровУслуг") Или ТипЗнч(ВыборкаПоДокументу.Ссылка) = Тип("ДокументСсылка.АктВыполненныхРабот")
							ИЛИ ТипЗнч(ВыборкаПоДокументу.Ссылка) = Тип("ДокументСсылка.ОтчетДавальцу") Тогда							

							Если СтавкаНДС = Неопределено Тогда
								СтавкаНДС = ДополнительныйСервер_Локализация.ПолучитьСтавкуНДС(Запись.СтавкаНДС);	
							КонецЕсли;
							Запись.БазаНДСУпр = Запись.СуммаСНДСУпр * 100 / (100 + СтавкаНДС);
							Запись.СуммаНДСУпр = Запись.СуммаНДСРегл / КурсВалютыУпр;
							Запись.СуммаБезНДСУпр  = Запись.СуммаСНДСУпр - Запись.СуммаНДСУпр;
						Иначе
							Запись.БазаНДСУпр = Запись.БазаНДСРегл / КурсВалютыУпр;
							Запись.СуммаНДСУпр = Запись.СуммаНДСРегл / КурсВалютыУпр;
							Запись.СуммаБезНДСУпр  = Запись.СуммаСНДСУпр - Запись.СуммаНДСУпр;
						КонецЕсли;
						// }
						// 4D

					КонецЕсли;

				КонецЦикла;
			КонецЦикла;
		КонецЦикла;

		НаборЗаписей = РегистрыСведений.СуммыДокументовВВалютеРегл.СоздатьНаборЗаписей();
		НаборЗаписей.Отбор.Регистратор.Установить(ВыборкаПоДокументу.Ссылка);
		НаборЗаписей.Загрузить(ТаблицаСуммыДокумента);
		НаборЗаписей.Записать();

		ТаблицаСуммыДокумента.Очистить();

	КонецЦикла;

	ОбновитьДвиженияДокументов(ДокументыКПересчету);

	Если ТипРасчетов = "РасчетыСКлиентами" Тогда
		СформироватьЗаданияКРаспределениюНДС(ДокументыКПересчету);
	КонецЕсли;

КонецПроцедуры
