﻿// 4D:ТК АНРЭКС, МаксимК, 18.06.2021 10:05:30 
// РКО, №29588
// {
&НаКлиенте
Процедура ТК_Чд_ВыдатьСотрудникПриИзмененииПосле(Элемент)
	Чд_ВыдатьСотрудникПриИзмененииНаСервере();
КонецПроцедуры

&НаСервере
Процедура Чд_ВыдатьСотрудникПриИзмененииНаСервере()
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	ДокументыФизическихЛиц.Серия КАК Серия,
		|	ДокументыФизическихЛиц.Номер КАК Номер,
		|	ДокументыФизическихЛиц.ВидДокумента КАК ВидДокумента,
		|	ДокументыФизическихЛиц.Физлицо.Представление КАК ФИО
		|ИЗ
		|	РегистрСведений.ДокументыФизическихЛиц КАК ДокументыФизическихЛиц
		|ГДЕ
		|	ДокументыФизическихЛиц.ВидДокумента = &ВидДокумента
		|	И ДокументыФизическихЛиц.Физлицо = &Физлицо";
	
	Запрос.УстановитьПараметр("ВидДокумента", Справочники.ВидыДокументовФизическихЛиц.ПаспортРФ);
	Запрос.УстановитьПараметр("Физлицо", Объект.Чд_ВыдатьСотрудник.ФизическоеЛицо);
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Пока Выборка.Следующий() Цикл
	    Объект.ПоДокументу = Строка(Выборка.ВидДокумента) + " " + Выборка.Серия  + Выборка.Номер;
		Объект.Выдать = СклонениеПредставленийОбъектов.ПросклонятьФИО(Выборка.ФИО, 3,,2);
		Возврат;
	КонецЦикла;
	
КонецПроцедуры
// }
// 4D