﻿
&НаСервере
Процедура ТК_ПриСозданииНаСервереПосле(Отказ, СтандартнаяОбработка)
	МассивРеквизитов = Новый Массив; 
	Реквизит = Новый РеквизитФормы("ЧД_ПервичныйДокумент", Новый ОписаниеТипов("ДокументСсылка.ПервичныйДокумент"), "Объект.Товары", "Первичный Документ", Истина);
	МассивРеквизитов.Добавить(Реквизит);
	ИзменитьРеквизиты(МассивРеквизитов);
	
	Для Каждого Товар из Объект.Товары Цикл
		Для Каждого Партия из Объект.ДетализацияПартий Цикл
			Если  Товар.ИдентификаторСтроки = Партия.ИдентификаторСтроки Тогда
				Товар.ЧД_ПервичныйДокумент = Партия.ДокументПоступления;
				Прервать;
			КонецЕсли;
		КонецЦикла;
	КонецЦикла;
	
	НовыйЭлемент = ЭтаФорма.Элементы.Добавить("ПервичныйДокумент", Тип("ПолеФормы"), Элементы.СобственныеТовары);
	НовыйЭлемент.Вид = ВидПоляФормы.ПолеВвода;
	НовыйЭлемент.ПутьКДанным = "Объект.Товары.ЧД_ПервичныйДокумент";
		
		
КонецПроцедуры

